#include <stdlib.h>
#include <string.h>

#include "ei_widgetclass.h"
#include "ei_widgetfunctions.h"


ei_widgetclass_t *class_register = NULL;



void ei_widgetclass_register(ei_widgetclass_t* widgetclass)
{
    if (class_register == NULL) class_register = widgetclass;
    else {
        ei_widgetclass_t *current = class_register;
        while (current->next != NULL) {
            current = current->next;
        }
        current->next = widgetclass;
    }
}

ei_widgetclass_t* ei_widgetclass_from_name(ei_widgetclass_name_t name)
{
    ei_widgetclass_t *current = class_register;
    while (current != NULL) {
        if (strcmp(current->name, name) == 0) {
            return current;
        }
        current = current->next;
    }
    return NULL;
}

void ei_frame_register_class()
{
    ei_widgetclass_t *frame_class = malloc(sizeof(ei_widgetclass_t));
    strcpy(frame_class->name, CLASS_NAME_FRAME);
    frame_class->allocfunc = ei_alloc_frame;
    frame_class->drawfunc = ei_draw_frame;
    frame_class->releasefunc = ei_release_frame;
    frame_class->setdefaultsfunc = ei_setdefaults_frame;
    frame_class->handlefunc = ei_sethandlefunc_frame;
    frame_class->geomnotifyfunc = ei_setgeomnotifyfunc_frame;
    frame_class->next = NULL;
    ei_widgetclass_register(frame_class);
}

void ei_button_register_class()
{
    ei_widgetclass_t *button_class = malloc(sizeof(ei_widgetclass_t));
    strcpy(button_class->name, CLASS_NAME_BUTTON);
    button_class->allocfunc = ei_alloc_button;
    button_class->drawfunc = ei_draw_button;
    button_class->releasefunc = ei_release_button;
    button_class->setdefaultsfunc = ei_setdefaults_button;
    button_class->handlefunc = ei_sethandlefunc_button;
    button_class->geomnotifyfunc = ei_setgeomnotifyfunc_button;
    button_class->next = NULL;
    ei_widgetclass_register(button_class);
}

/**
 * \brief	Registers the "resize" widget class in the program. This must be called only
 *		once before widgets of the class "resize" can be created and configured with
 *		\ref ei_resize_configure.
 *              This function is called by \ref ei_toplevel_register_class.
 */
void ei_resize_register_class()
{
    ei_widgetclass_t *resize_class = malloc(sizeof(ei_widgetclass_t));
    strcpy(resize_class->name, CLASS_NAME_RESIZE);
    resize_class->allocfunc = ei_alloc_resize;
    resize_class->drawfunc = ei_draw_resize;
    resize_class->releasefunc = ei_release_resize;
    resize_class->setdefaultsfunc = ei_setdefaults_resize;
    resize_class->handlefunc = ei_sethandlefunc_resize;
    resize_class->geomnotifyfunc = ei_setgeomnotifyfunc_resize;
    resize_class->next = NULL;
    ei_widgetclass_register(resize_class);
}

void ei_toplevel_register_class()
{
    ei_widgetclass_t *toplevel_class = malloc(sizeof(ei_widgetclass_t));
    strcpy(toplevel_class->name, CLASS_NAME_TOP_LEVEL);
    toplevel_class->allocfunc = ei_alloc_top_level;
    toplevel_class->drawfunc = ei_draw_top_level;
    toplevel_class->releasefunc = ei_release_top_level;
    toplevel_class->setdefaultsfunc = ei_setdefaults_top_level;
    toplevel_class->handlefunc = ei_sethandlefunc_top_level;
    toplevel_class->geomnotifyfunc = ei_setgeomnotifyfunc_top_level;
    toplevel_class->next = NULL;
    ei_widgetclass_register(toplevel_class);
    ei_resize_register_class();
}



