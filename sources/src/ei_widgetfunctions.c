#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "ei_application.h"
#include "ei_draw.h"
#include "ei_event.h"
#include "ei_placer.h"
#include "ei_shape.h"
#include "ei_tools.h"
#include "ei_types.h"
#include "ei_utils.h"
#include "ei_widget.h"
#include "ei_widgetclasses.h"
#include "ei_widgetfunctions.h"


static const char* k_default_top_level_title = "TopLevel";



/**
 * \brief   Set parameters commons to all widgets.
 * 
 * @param       widget      The widget to set.
 */
void ei_setdefaults_widget(struct ei_widget_t* widget)
{
    widget->pick_id = -1;
    if (widget->pick_color != NULL) {
        free(widget->pick_color);
    }
    widget->pick_color = NULL;
    widget->parent = NULL;
    widget->children_head = NULL;
    widget->children_tail = NULL;
    widget->next_sibling = NULL;
    widget->placer_params = NULL;
    widget->screen_location = ei_rect(ei_point(0, 0), ei_size(0, 0));
    widget->content_rect = &widget->screen_location;
}

/**
 * \brief   Caculate where to place the text in the widget.
 *    
 * @param       widget          The widget to place the text in.
 * @param       text            Text to write.
 * @param       font            Font of text.
 * @param       text_anchor     Anchor wanted to place the text.
 * 
 * @return  The top left point location of the text.
 */
ei_point_t where_text(const ei_widget_t *widget, const char *text, ei_font_t font, ei_anchor_t text_anchor)
{
    ei_point_t where = widget->screen_location.top_left;
    int width_text, height_text;
    hw_text_compute_size(text, font, &width_text, &height_text);
    switch (text_anchor) {
    case ei_anc_none:
        break;
    case ei_anc_northwest:
        break;
    case ei_anc_north:
        where.x += widget->screen_location.size.width / 2;
        where.x -= width_text / 2;
        break;
    case ei_anc_northeast:
        where.x += widget->screen_location.size.width;
        where.x -= width_text;
        break;
    case ei_anc_southwest:
        where.y += widget->screen_location.size.height;
        where.y -= height_text;
        break;
    case ei_anc_south:
        where.y += widget->screen_location.size.height / 2;
        where.y -= height_text / 2;
        break;
    case ei_anc_southeast:
        where.x += widget->screen_location.size.width;
        where.x -= width_text;
        where.y += widget->screen_location.size.height;
        where.y -= height_text;
    case ei_anc_center:
        where.x += widget->screen_location.size.width / 2;
        where.x -= width_text / 2;
        where.y += widget->screen_location.size.height / 2;
        where.y -= height_text / 2;
        break;
    default:
        exit(EXIT_FAILURE);
    }
    return where;
}


/* ------------------------------------------------------------------------- */
/* ///////////////////////////////// FRAME \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */
/* ------------------------------------------------------------------------- */

void* ei_alloc_frame()
{
    return calloc(1, sizeof(ei_frame_widget_t));
}

void ei_release_frame(struct ei_widget_t* widget)
{
    ei_frame_widget_t *frame = (ei_frame_widget_t *) widget;
    if (frame->text != NULL) free(frame->text); // PAS SUR -> le text n'est pas malloc mais créé dans le main...
    if (frame->img_rect != NULL) free(frame->img_rect); // PAS SUR -> idem je pense
}

void ei_draw_frame(struct ei_widget_t* widget,
                   ei_surface_t surface,
                   ei_surface_t pick_surface,
                   ei_rect_t* clipper)
{
    ei_frame_widget_t *frame = (ei_frame_widget_t *)widget;
    if(frame->border_width == 0){
        // No border (like the root widget)
        ei_linked_point_t *to_draw = ei_get_rounded_frame(widget->screen_location, 0, ALL);

        if(frame->img_rect) ei_copy_surface(surface, &widget->screen_location, frame->img, frame->img_rect, EI_FALSE);
        else ei_draw_polygon(surface, to_draw, frame->color, clipper);

        if(frame->text != NULL){
            ei_point_t where = where_text(widget, frame->text, frame->text_font, frame->text_anchor);
            ei_draw_text(surface, &where, frame->text, frame->text_font, &frame->text_color, clipper);
        }
        // Draws on the offscreen
        ei_draw_polygon(pick_surface, to_draw, *widget->pick_color, clipper);

        free_linked_point(to_draw);
        return;
    }

    ei_point_t to_top_left_smaller = ei_point(frame->border_width, frame->border_width);
    ei_point_t top_left_smaller = ei_point_add(widget->screen_location.top_left, to_top_left_smaller);
    ei_size_t smaller_size = ei_size_sub(widget->screen_location.size, ei_point_as_size(ei_point_add(to_top_left_smaller, to_top_left_smaller)));
    ei_rect_t smaller_rect = ei_rect(top_left_smaller, smaller_size);

    if(frame->relief == ei_relief_none){
        // No relief
        ei_linked_point_t *big = ei_get_rounded_frame(widget->screen_location, 0, ALL);
        ei_linked_point_t *small = ei_get_rounded_frame(smaller_rect, 0, ALL);
        ei_color_t border_color = {0, 0, 0, 255};

        ei_draw_polygon(surface, big, border_color, clipper);

        if(frame->img_rect) ei_copy_surface(surface, &smaller_rect, frame->img, frame->img_rect, EI_FALSE);
        else ei_draw_polygon(surface, small, frame->color, clipper);

        if(frame->text != NULL){
            ei_point_t where = where_text(widget, frame->text, frame->text_font, frame->text_anchor);
            ei_draw_text(surface, &where, frame->text, frame->text_font, &frame->text_color, clipper);
        }
        // Draws on the offscreen
        ei_draw_polygon(pick_surface, big, *widget->pick_color, clipper);

        free(big);
        free(small);
        return;
    }
    ei_color_t color = frame->color, color_top, color_bottom,
                color1 = {
                    color.red - 60*color.red/100,
                    color.green - 60*color.green/100,
                    color.blue - 60*color.blue/100,
                    color.alpha
                },
                color2 = {
                    255 - color1.red,
                    255 - color1.green,
                    255 - color1.blue,
                    color1.alpha
                };
    if(frame->relief == ei_relief_raised){
        color_top = color2;
        color_bottom = color1;
    }
    else if(frame->relief == ei_relief_sunken){
        color_top = color1;
        color_bottom = color2;
    }
    ei_linked_point_t *top = ei_get_rounded_frame(widget->screen_location, 0, TOP);
    ei_linked_point_t *bottom = ei_get_rounded_frame(widget->screen_location, 0, BOTTOM);
    ei_linked_point_t *center = ei_get_rounded_frame(smaller_rect, 0, ALL);

    ei_draw_polygon(surface, top, color_top, clipper);
    ei_draw_polygon(surface, bottom, color_bottom, clipper);

    if(frame->img_rect) ei_copy_surface(surface, &smaller_rect, frame->img, frame->img_rect, EI_FALSE);
    else ei_draw_polygon(surface, center, color, clipper);

    if(frame->text != NULL){
        ei_point_t where = where_text(widget, frame->text, frame->text_font, frame->text_anchor);
        ei_draw_text(surface, &where, frame->text, frame->text_font, &frame->text_color, clipper);
    }

    // Draws on the offscreen
    ei_draw_polygon(pick_surface, top, *widget->pick_color, clipper);
    ei_draw_polygon(pick_surface, bottom, *widget->pick_color, clipper);

    free_linked_point(top);
    free_linked_point(bottom);
    free_linked_point(center);
}

void ei_setdefaults_frame(struct ei_widget_t* widget)
{
    ei_setdefaults_widget(widget);
    widget->wclass = ei_widgetclass_from_name(CLASS_NAME_FRAME);
    ei_frame_widget_t *frame = (ei_frame_widget_t *) widget;
    widget->requested_size = ei_size(-1, -1);
    frame->color = ei_default_background_color;
    frame->border_width = 0;
    frame->relief = ei_relief_none;
    frame->text = NULL;
    frame->text_font = ei_default_font;
    frame->text_color = ei_font_default_color;
    frame->text_anchor = ei_anc_center;
    frame->img = NULL;
    frame->img_rect = NULL;
    frame->img_anchor = ei_anc_center;
}

ei_bool_t ei_sethandlefunc_frame(struct ei_widget_t* widget,
                                 struct ei_event_t* event)
{
    ei_widget_t *last_active = ei_event_get_active_widget();

    switch (event->type) {
    case ei_ev_mouse_buttondown:
        ei_event_set_active_widget(widget);
        return EI_TRUE;
    case ei_ev_mouse_buttonup:
        ei_event_set_active_widget(NULL);
        if (last_active != NULL) {
            last_active->wclass->handlefunc(last_active, event);
//            ei_app_invalidate_rect(&last_active->screen_location);
        }
        return EI_TRUE;
    case ei_ev_mouse_move:
        if (last_active != NULL && last_active != widget) {
            last_active->wclass->handlefunc(last_active, event);
//            ei_app_invalidate_rect(&last_active->screen_location);
            return EI_TRUE;
        }
    default:
        return EI_FALSE;
    }

}

void ei_setgeomnotifyfunc_frame(struct ei_widget_t* widget,
                                ei_rect_t rect)
{
    // Old screen location need to be redrawn
    ei_app_invalidate_rect(&widget->screen_location);
    widget->screen_location = rect;
    // New screen location too
    ei_app_invalidate_rect(&widget->screen_location);
}


/* ------------------------------------------------------------------------- */
/* //////////////////////////////// BUTTON  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */
/* ------------------------------------------------------------------------- */

void* ei_alloc_button()
{
    return calloc(1, sizeof(ei_button_widget_t));
}

void ei_release_button(struct ei_widget_t* widget)
{
    ei_button_widget_t *button = (ei_button_widget_t *)widget;
    if(button->text != NULL) free(button->text);
    if(button->img_rect != NULL) free(button->img_rect);
}

void ei_draw_button(struct ei_widget_t*	widget,
                   ei_surface_t         surface,
                   ei_surface_t         pick_surface,
                   ei_rect_t*		clipper)
{
    ei_button_widget_t *button = (ei_button_widget_t *)widget;
    ei_rect_t sl_minus_corner_rad = ei_rect(
            ei_point_add(widget->screen_location.top_left, ei_point(button->corner_radius, button->corner_radius)),
            ei_size(widget->screen_location.size.width - 2*button->corner_radius, widget->screen_location.size.height - 2*button->corner_radius)
    );
    if(button->border_width == 0){
        // No border (like the root widget)
        ei_linked_point_t *to_draw = ei_get_rounded_frame(sl_minus_corner_rad, button->corner_radius, ALL);

        if(button->img_rect) ei_copy_surface(surface, &sl_minus_corner_rad, button->img, button->img_rect, EI_FALSE);
        else ei_draw_polygon(surface, to_draw, button->color, clipper);

        if(button->text != NULL){
            ei_point_t where = where_text(widget, button->text, button->text_font, button->text_anchor);
            ei_draw_text(surface, &where, button->text, button->text_font, &button->text_color, clipper);
        }
        // Draws on the offscreen
        ei_draw_polygon(pick_surface, to_draw, *widget->pick_color, clipper);

        free_linked_point(to_draw);
        return;
    }

    ei_point_t to_top_left_smaller = ei_point(button->border_width, button->border_width);
    ei_point_t top_left_smaller = ei_point_add(sl_minus_corner_rad.top_left, to_top_left_smaller);
    ei_size_t smaller_size = ei_size_sub(sl_minus_corner_rad.size, ei_point_as_size(ei_point_add(to_top_left_smaller, to_top_left_smaller)));
    ei_rect_t smaller_rect = ei_rect(top_left_smaller, smaller_size);

    if(button->relief == ei_relief_none){
        // No relief
        ei_linked_point_t *big = ei_get_rounded_frame(sl_minus_corner_rad, button->corner_radius, ALL);
        ei_linked_point_t *small = ei_get_rounded_frame(smaller_rect, button->corner_radius, ALL);
        ei_color_t border_color = {0, 0, 0, 255};

        ei_draw_polygon(surface, big, border_color, clipper);

        if(button->img_rect){
            ei_rect_t rect_image = ei_rect_intersect(&smaller_rect, clipper);
            ei_rect_t* image_rect_ptr = button->img_rect;
            int new_x = button->img_rect->top_left.x + minus(rect_image.top_left.x, smaller_rect.top_left.x);
            int new_y = button->img_rect->top_left.y + minus(rect_image.top_left.y, smaller_rect.top_left.y);
            ei_rect_t image_rect = {
                {new_x, new_y}, rect_image.size};
            image_rect_ptr = &image_rect;

            ei_copy_surface(surface, &rect_image, button->img, image_rect_ptr, EI_FALSE);
        }else ei_draw_polygon(surface, small, button->color, clipper);

        if(button->text != NULL){
            ei_point_t where = where_text(widget, button->text, button->text_font, button->text_anchor);
            ei_draw_text(surface, &where, button->text, button->text_font, &button->text_color, clipper);
        }
        // Draws on the offscreen
        ei_draw_polygon(pick_surface, big, *widget->pick_color, clipper);

        free(big);
        free(small);
        return;
    }
    ei_color_t color = button->color, color_top, color_bottom,
                color1 = {
                    color.red - 60*color.red/100,
                    color.green - 60*color.green/100,
                    color.blue - 60*color.blue/100,
                    color.alpha
                },
                color2 = {
                    255 - color1.red,
                    255 - color1.green,
                    255 - color1.blue,
                    color1.alpha
                };
    if(button->relief == ei_relief_raised){
        color_top = color2;
        color_bottom = color1;
    }
    else if(button->relief == ei_relief_sunken){
        color_top = color1;
        color_bottom = color2;
    }
    ei_linked_point_t *top = ei_get_rounded_frame(sl_minus_corner_rad, button->corner_radius, TOP);
    ei_linked_point_t *bottom = ei_get_rounded_frame(sl_minus_corner_rad, button->corner_radius, BOTTOM);
    ei_linked_point_t *center = ei_get_rounded_frame(smaller_rect, button->corner_radius, ALL);

    ei_draw_polygon(surface, top, color_top, clipper);
    ei_draw_polygon(surface, bottom, color_bottom, clipper);

    if(button->img_rect){
        ei_rect_t rect_image = ei_rect_intersect(&smaller_rect, clipper);
        ei_rect_t* image_rect_ptr = button->img_rect;
        int new_x = button->img_rect->top_left.x + minus(rect_image.top_left.x, smaller_rect.top_left.x);
        int new_y = button->img_rect->top_left.y + minus(rect_image.top_left.y, smaller_rect.top_left.y);
        //hw_surface_set_origin(button->img, button->img_rect->top_left);
        ei_rect_t image_rect = {
            {new_x, new_y}, rect_image.size};
        image_rect_ptr = &image_rect;

        ei_copy_surface(surface, &rect_image, button->img, image_rect_ptr, EI_FALSE);
    }else ei_draw_polygon(surface, center, color, clipper);

    if(button->text != NULL){
        ei_point_t where = where_text(widget, button->text, button->text_font, button->text_anchor);
        ei_draw_text(surface, &where, button->text, button->text_font, &button->text_color, clipper);
    }

    // Draws on the offscreen
    ei_draw_polygon(pick_surface, top, *widget->pick_color, clipper);
    ei_draw_polygon(pick_surface, bottom, *widget->pick_color, clipper);

    free_linked_point(top);
    free_linked_point(bottom);
    free_linked_point(center);
}

void ei_setdefaults_button(struct ei_widget_t* widget)
{
    ei_setdefaults_widget(widget);
    widget->wclass = ei_widgetclass_from_name(CLASS_NAME_BUTTON);
    ei_button_widget_t *button = (ei_button_widget_t *) widget;
    widget->requested_size = ei_size(-1, -1);
    button->color = ei_default_background_color;
    button->border_width = k_default_button_border_width;
    button->corner_radius = k_default_button_corner_radius;
    button->relief = ei_relief_raised;
    button->text = NULL;
    button->text_font = ei_default_font;
    button->text_color = ei_font_default_color;
    button->text_anchor = ei_anc_center;
    button->img = NULL;
    button->img_rect = NULL;
    button->img_anchor = ei_anc_center;
    button->callback = NULL;
    button->user_param = NULL;
}

ei_bool_t ei_sethandlefunc_button(struct ei_widget_t* widget,
                                  struct ei_event_t* event)
{
    ei_button_widget_t *button = (ei_button_widget_t *) widget;
    ei_widget_t *last_active = ei_event_get_active_widget();

    switch (event->type) {
    case ei_ev_mouse_buttondown:
        if (event->param.mouse.button_number == EI_MOUSEBUTTON_LEFT) {
            ei_event_set_active_widget(widget);
            ei_relief_t new_relief = ei_relief_sunken;
            ei_button_configure(widget, NULL, NULL, NULL, NULL, &new_relief, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
            // Notify to redraw the button
//            ei_app_invalidate_rect(&widget->screen_location);
            return EI_TRUE;
        }
    case ei_ev_mouse_buttonup:
        ei_event_set_active_widget(NULL);
        if (event->param.mouse.button_number == EI_MOUSEBUTTON_LEFT) {
            ei_relief_t new_relief = ei_relief_raised;
            ei_button_configure(widget, NULL, NULL, NULL, NULL, &new_relief, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
//            ei_app_invalidate_rect(&button->widget.screen_location);
            if (last_active == widget) {
                if (button->callback != NULL) {
                    button->callback(widget, event, button->user_param);
//                    ei_app_invalidate_rect(&button->widget.screen_location);
//                    ei_app_invalidate_rect(&ei_app_root_widget()->screen_location);
//                    ei_placer_run(widget);
                }
            }
            else if (last_active != NULL) {
                last_active->wclass->handlefunc(last_active, event);
            }
        }
        return EI_TRUE;
    case ei_ev_mouse_move:
        if (last_active != NULL && last_active != widget) {
            last_active->wclass->handlefunc(last_active, event);
//            ei_app_invalidate_rect(&last_active->screen_location);
            return EI_TRUE;
        }
    default:
        return EI_FALSE;
    }
    
}

void ei_setgeomnotifyfunc_button(struct ei_widget_t* widget,
                                 ei_rect_t rect)
{
    // Old screen location need to be redrawn
    ei_app_invalidate_rect(&widget->screen_location);
    widget->screen_location = rect;
    // New screen location too
    ei_app_invalidate_rect(&widget->screen_location);
}


/* ------------------------------------------------------------------------- */
/* /////////////////////////////// TOP LEVEL \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */
/* ------------------------------------------------------------------------- */

void* ei_alloc_top_level()
{
    return calloc(1, sizeof(ei_top_level_widget_t));
}

void ei_release_top_level(struct ei_widget_t* widget)
{
    ei_top_level_widget_t *top_level = (ei_top_level_widget_t*) widget;
    if (top_level->title != NULL) free(top_level->title);
    if (top_level->min_size != NULL) free(top_level->min_size);
    if (top_level->button_close != NULL) ei_widget_destroy(top_level->button_close);
    if (top_level->resize != NULL) ei_widget_destroy(top_level->resize);
}

void ei_draw_top_level(struct ei_widget_t* widget,
                       ei_surface_t surface,
                       ei_surface_t pick_surface,
                       ei_rect_t* clipper)
{
    ei_top_level_widget_t *top_level = (ei_top_level_widget_t*) widget;

    ei_point_t top_left = top_level->widget.screen_location.top_left;
    ei_size_t size;
    if (top_level->widget.screen_location.size.width >= 0 && top_level->widget.screen_location.size.height >= 0) {
        size = top_level->widget.screen_location.size;
    }
    else {
        size = top_level->widget.requested_size;
    }

    // Title bar
    ei_size_t size_bar = ei_size(size.width, k_default_toplevel_titlebar_height);
    ei_rect_t rect_bar = ei_rect(top_left, size_bar);
    ei_linked_point_t *points_bar = ei_get_bar_title(&rect_bar);
    ei_draw_polygon(surface, points_bar, k_default_toplevel_titlebar_color, clipper);

    // Text inside the title bar
    ei_point_t where_title = ei_point_add(top_left, ei_point(50, 0));
    ei_rect_t title_rec = rect_bar;
    title_rec.size.width -= 10;
    ei_rect_t clip_text = ei_rect_intersect(&title_rec, clipper);
    ei_draw_text(surface, &where_title, top_level->title, NULL, &ei_font_default_color, &clip_text);

    // Only draws the title bar on the offscreen which is the only clickable area of a toplevel to move it
    ei_draw_polygon(pick_surface, points_bar, *widget->pick_color, clipper);
    free_linked_point(points_bar);

    // Rectangle to make the border
    if (top_level->border_width != 0) {
        ei_point_t top_left_border = ei_point(top_left.x, top_left.y + k_default_toplevel_titlebar_height);
        ei_size_t size_border = ei_size_sub(size, ei_size(0, k_default_toplevel_titlebar_height));
        ei_rect_t rect_border = ei_rect(top_left_border, size_border);
        ei_linked_point_t *border = ei_get_rectangle_frame(&rect_border);
        ei_draw_polygon(surface, border, k_default_toplevel_titlebar_color, clipper);
        free_linked_point(border);
    }

    // Rectangle without decoration
    ei_point_t top_left_main = ei_point_add(top_left, ei_point(top_level->border_width, k_default_toplevel_titlebar_height));
    ei_size_t size_main = ei_size(size.width - 2 * top_level->border_width, size.height - k_default_toplevel_titlebar_height - top_level->border_width);
    ei_rect_t rect_main = ei_rect(top_left_main, size_main);
    ei_linked_point_t *rectangle = ei_get_rectangle_frame(&rect_main);
    // Draws on the screen
    ei_draw_polygon(surface, rectangle, top_level->color, clipper);
    free_linked_point(rectangle);

    // Close button
    if (top_level->button_close != NULL) {
        ei_placer_run(top_level->button_close); // cause the parent was modified
        top_level->button_close->wclass->drawfunc(top_level->button_close, surface, pick_surface, clipper);
    }

    // Resize button
    if (top_level->resize != NULL) {
        ei_placer_run(top_level->resize); // cause the parent was modified
        top_level->resize->wclass->drawfunc(top_level->resize, surface, pick_surface, clipper);
    }
}

void ei_setdefaults_top_level(struct ei_widget_t* widget)
{
    ei_setdefaults_widget(widget);
    widget->wclass = ei_widgetclass_from_name(CLASS_NAME_TOP_LEVEL);
    ei_top_level_widget_t *top_level = (ei_top_level_widget_t *) widget;
    widget->requested_size = ei_size(320, 240);
    top_level->color = ei_default_background_color;
    top_level->border_width = 4;
    if (top_level->title != NULL) {
        free(top_level->title);
    }
    top_level->title = malloc(sizeof(char)*(strlen(k_default_top_level_title) + 1));
    strcpy(top_level->title, k_default_top_level_title);
    top_level->closable = EI_TRUE;
    top_level->resizable = ei_axis_both;
    if (top_level->min_size != NULL) {
        free(top_level->min_size);
    }
    top_level->min_size = malloc(sizeof(ei_size_t));
    *(top_level->min_size) = ei_size(160, 120);
}

ei_point_t old_mouse_position_top_level;

ei_bool_t ei_sethandlefunc_top_level(struct ei_widget_t* widget,
                                     struct ei_event_t* event)
{
    ei_widget_t *last_active = ei_event_get_active_widget();

    switch (event->type) {
    case ei_ev_mouse_buttondown:
        ei_event_set_active_widget(widget);
        old_mouse_position_top_level = event->param.mouse.where;
        return EI_TRUE;
    case ei_ev_mouse_buttonup:
        ei_event_set_active_widget(NULL);
        // Put the top level above the others
        if (last_active != NULL && last_active == widget) {
            ei_widget_t *parent = widget->parent;
            if (parent->children_head != parent->children_tail) {
                ei_widget_t *child = remove_from_parent(widget);
                parent->children_tail->next_sibling = child;
                parent->children_tail = child;
                ei_app_invalidate_rect(&widget->screen_location);
            }
        }
        if (last_active != NULL) {
            last_active->wclass->handlefunc(last_active, event);
//            ei_app_invalidate_rect(&last_active->screen_location);
        }
        return EI_TRUE;
    case ei_ev_mouse_move:
        // Put the top level above the others
        if (last_active != NULL && last_active == widget) {
            ei_widget_t *parent = widget->parent;
            if (parent->children_head != parent->children_tail) {
                ei_widget_t *child = remove_from_parent(widget);
                parent->children_tail->next_sibling = child;
                parent->children_tail = child;
                ei_app_invalidate_rect(&widget->screen_location);
            }
        }
        if (last_active != NULL && last_active != widget) {
            last_active->wclass->handlefunc(last_active, event);
//            ei_app_invalidate_rect(&last_active->screen_location);
            return EI_TRUE;
        }
        else if (ei_event_get_active_widget() == widget) {
//            ei_rect_t old_screen_location = widget->screen_location;
            // Mouse pointer position in relation to root coordinate
            ei_point_t where = event->param.mouse.where;
            // Update absolute position in placer params
            ei_point_t new_location = ei_point_add(
                    widget->screen_location.top_left,
                    ei_point(where.x - old_mouse_position_top_level.x, where.y - old_mouse_position_top_level.y)
                    );
            old_mouse_position_top_level = event->param.mouse.where;
            widget->placer_params->rx = NULL;
            widget->placer_params->ry = NULL;
            widget->placer_params->x_data = new_location.x;
            widget->placer_params->x = &widget->placer_params->x_data;
            widget->placer_params->y_data = new_location.y;
            widget->placer_params->y = &widget->placer_params->y_data;
            // To notify to redraw the top_level
//            ei_app_invalidate_rect(&widget->screen_location);
            // To replace the pixels where the top_level is not anymore
//            ei_app_invalidate_rect(&old_screen_location);
            return EI_TRUE;
        }
    default:
        return EI_FALSE;
    }
}

void ei_setgeomnotifyfunc_top_level(struct ei_widget_t* widget,
                                    ei_rect_t rect)
{
    // Old screen location need to be redrawn
    ei_app_invalidate_rect(&widget->screen_location);
    ei_top_level_widget_t *top_level = (ei_top_level_widget_t *) widget;
    widget->screen_location = rect;
    ei_point_t top_left_main = ei_point_add(rect.top_left, ei_point(top_level->border_width, k_default_toplevel_titlebar_height));
    ei_size_t size_main = ei_size(rect.size.width - 2 * top_level->border_width, rect.size.height - k_default_toplevel_titlebar_height - top_level->border_width);
    ei_rect_t rect_main = ei_rect(top_left_main, size_main);
    if (widget->content_rect != &widget->screen_location) {
        free(widget->content_rect);
    }
    widget->content_rect = malloc(sizeof(ei_rect_t));
    *widget->content_rect = rect_main;
    // New screen location too
    ei_app_invalidate_rect(&widget->screen_location);
}


/* ------------------------------------------------------------------------- */
/* //////////////////////////////// RESIZE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */
/* ------------------------------------------------------------------------- */

void* ei_alloc_resize()
{
    return calloc(1, sizeof(ei_resize_widget_t));
}

void ei_release_resize(struct ei_widget_t* widget)
{
    ei_resize_widget_t *resize = (ei_resize_widget_t *) widget;
    if (resize->text != NULL) free(resize->text);
    if (resize->img_rect != NULL) free(resize->img_rect);
}

void ei_draw_resize(struct ei_widget_t* widget,
                    ei_surface_t surface,
                    ei_surface_t pick_surface,
                    ei_rect_t* clipper)
{
    ei_resize_widget_t *resize = (ei_resize_widget_t *) widget;
    ei_rect_t sl_minus_corner_rad = ei_rect(
            ei_point_add(widget->screen_location.top_left, ei_point(resize->corner_radius, resize->corner_radius)),
            ei_size(widget->screen_location.size.width - 2*resize->corner_radius, widget->screen_location.size.height - 2*resize->corner_radius)
    );
    if(resize->border_width == 0){
        // No border (like the root widget)
        ei_linked_point_t *to_draw = ei_get_rounded_frame(sl_minus_corner_rad, resize->corner_radius, ALL);

        if(resize->img_rect) ei_copy_surface(surface, &sl_minus_corner_rad, resize->img, resize->img_rect, EI_FALSE);
        else ei_draw_polygon(surface, to_draw, resize->color, clipper);

        if(resize->text != NULL){
            ei_point_t where = where_text(widget, resize->text, resize->text_font, resize->text_anchor);
            ei_draw_text(surface, &where, resize->text, resize->text_font, &resize->text_color, clipper);
        }
        // Draws on the offscreen
        ei_draw_polygon(pick_surface, to_draw, *widget->pick_color, clipper);

        free_linked_point(to_draw);
        return;
    }

    ei_point_t to_top_left_smaller = ei_point(resize->border_width, resize->border_width);
    ei_point_t top_left_smaller = ei_point_add(sl_minus_corner_rad.top_left, to_top_left_smaller);
    ei_size_t smaller_size = ei_size_sub(sl_minus_corner_rad.size, ei_point_as_size(ei_point_add(to_top_left_smaller, to_top_left_smaller)));
    ei_rect_t smaller_rect = ei_rect(top_left_smaller, smaller_size);

    if(resize->relief == ei_relief_none){
        // No relief
        ei_linked_point_t *big = ei_get_rounded_frame(sl_minus_corner_rad, resize->corner_radius, ALL);
        ei_linked_point_t *small = ei_get_rounded_frame(smaller_rect, resize->corner_radius, ALL);
        ei_color_t border_color = {0, 0, 0, 255};

        ei_draw_polygon(surface, big, border_color, clipper);

        if(resize->img_rect) ei_copy_surface(surface, &smaller_rect, resize->img, resize->img_rect, EI_FALSE);
        else ei_draw_polygon(surface, small, resize->color, clipper);

        if(resize->text != NULL){
            ei_point_t where = where_text(widget, resize->text, resize->text_font, resize->text_anchor);
            ei_draw_text(surface, &where, resize->text, resize->text_font, &resize->text_color, clipper);
        }
        // Draws on the offscreen
        ei_draw_polygon(pick_surface, big, *widget->pick_color, clipper);

        free(big);
        free(small);
        return;
    }
    ei_color_t color = resize->color, color_top, color_bottom,
                color1 = {
                    color.red - 60*color.red/100,
                    color.green - 60*color.green/100,
                    color.blue - 60*color.blue/100,
                    color.alpha
                },
                color2 = {
                    255 - color1.red,
                    255 - color1.green,
                    255 - color1.blue,
                    color1.alpha
                };
    if(resize->relief == ei_relief_raised){
        color_top = color2;
        color_bottom = color1;
    }
    else if(resize->relief == ei_relief_sunken){
        color_top = color1;
        color_bottom = color2;
    }
    ei_linked_point_t *top = ei_get_rounded_frame(sl_minus_corner_rad, resize->corner_radius, TOP);
    ei_linked_point_t *bottom = ei_get_rounded_frame(sl_minus_corner_rad, resize->corner_radius, BOTTOM);
    ei_linked_point_t *center = ei_get_rounded_frame(smaller_rect, resize->corner_radius, ALL);

    ei_draw_polygon(surface, top, color_top, clipper);
    ei_draw_polygon(surface, bottom, color_bottom, clipper);

    if(resize->img_rect) ei_copy_surface(surface, &smaller_rect, resize->img, resize->img_rect, EI_FALSE);
    else ei_draw_polygon(surface, center, color, clipper);

    if(resize->text != NULL){
        ei_point_t where = where_text(widget, resize->text, resize->text_font, resize->text_anchor);
        ei_draw_text(surface, &where, resize->text, resize->text_font, &resize->text_color, clipper);
    }

    // Draws on the offscreen
    ei_draw_polygon(pick_surface, top, *widget->pick_color, clipper);
    ei_draw_polygon(pick_surface, bottom, *widget->pick_color, clipper);

    free_linked_point(top);
    free_linked_point(bottom);
    free_linked_point(center);
}

void ei_setdefaults_resize(struct ei_widget_t* widget)
{
    ei_setdefaults_widget(widget);
    widget->wclass = ei_widgetclass_from_name(CLASS_NAME_RESIZE);
    ei_resize_widget_t *resize = (ei_resize_widget_t *) widget;
    widget->requested_size = ei_size(-1, -1);
    resize->color = ei_default_background_color;
    resize->border_width = k_default_button_border_width;
    resize->corner_radius = k_default_button_corner_radius;
    resize->relief = ei_relief_raised;
    resize->text = NULL;
    resize->text_font = ei_default_font;
    resize->text_color = ei_font_default_color;
    resize->text_anchor = ei_anc_center;
    resize->img = NULL;
    resize->img_rect = NULL;
    resize->img_anchor = ei_anc_center;
    resize->callback = NULL;
    resize->user_param = NULL;
}

ei_bool_t ei_sethandlefunc_resize(struct ei_widget_t* widget,
                                  struct ei_event_t* event)
{
//    ei_resize_widget_t *resize = (ei_resize_widget_t *) widget;
    ei_top_level_widget_t *top_level = (ei_top_level_widget_t*) widget->parent;
    ei_widget_t *last_active = ei_event_get_active_widget();

    switch (event->type) {
    case ei_ev_mouse_buttondown:
        if (event->param.mouse.button_number == EI_MOUSEBUTTON_LEFT) {
            ei_event_set_active_widget(widget);
            ei_relief_t new_relief = ei_relief_sunken;
            ei_button_configure(widget, NULL, NULL, NULL, NULL, &new_relief, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
//            ei_app_invalidate_rect(&widget->screen_location);
            return EI_TRUE;
        }
    case ei_ev_mouse_buttonup:
        ei_event_set_active_widget(NULL);
        if (event->param.mouse.button_number == EI_MOUSEBUTTON_LEFT) {
            if (last_active != NULL) {
                last_active->wclass->handlefunc(last_active, event);
//                ei_app_invalidate_rect(&last_active->screen_location);
            }
            ei_relief_t new_relief = ei_relief_raised;
            ei_button_configure(widget, NULL, NULL, NULL, NULL, &new_relief, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
//            ei_app_invalidate_rect(&resize->widget.screen_location);
        }
        return EI_TRUE;
    case ei_ev_mouse_move:
        if (top_level->resizable != ei_axis_none && last_active == widget) {
//            ei_rect_t old_screen_location = top_level->widget.screen_location;
            ei_rect_t new_screen_location = top_level->widget.screen_location;
            ei_point_t where = event->param.mouse.where;
            if (top_level->resizable == ei_axis_both || top_level->resizable == ei_axis_x) {                
                new_screen_location.size.width = max(where.x - new_screen_location.top_left.x, top_level->min_size->width);
            }
            if (top_level->resizable == ei_axis_both || top_level->resizable == ei_axis_y) {                
                new_screen_location.size.height = max(where.y - new_screen_location.top_left.y, top_level->min_size->height);
            }
            top_level->widget.placer_params->rw = NULL;
            top_level->widget.placer_params->rh = NULL;
            top_level->widget.placer_params->w_data = new_screen_location.size.width;
            top_level->widget.placer_params->w = &top_level->widget.placer_params->w_data;
            top_level->widget.placer_params->h_data = new_screen_location.size.height;
            top_level->widget.placer_params->h = &top_level->widget.placer_params->h_data;
            // To notify to redraw the top_level
//            ei_app_invalidate_rect(&top_level->widget.screen_location);
            // To replace the pixels where the top_level is not anymore
//            ei_app_invalidate_rect(&old_screen_location);
            return EI_TRUE;
        }
    default:
        return EI_FALSE;
    }
}

void ei_setgeomnotifyfunc_resize(struct ei_widget_t* widget,
                                 ei_rect_t rect)
{
    // Old screen location need to be redrawn
    ei_app_invalidate_rect(&widget->screen_location);
    widget->screen_location = rect;
    // New screen location too
    ei_app_invalidate_rect(&widget->screen_location);
}
