#include <stdlib.h>

#include "ei_tools.h"



int compare_points(ei_point_t point1, ei_point_t point2)
{
    if (point1.x - point2.x > 0) return 1;
    else if (point1.x - point2.x < 0) return -1;
    else {
        if (point1.y - point2.y > 0) return 1;
        else if (point1.y - point2.y < 0) return -1;
        else return 0;
    }
}

int compare_sizes(ei_size_t size1, ei_size_t size2)
{
    if (size1.width - size2.width > 0) return 1;
    else if (size1.width - size2.width < 0) return -1;
    else {
        if (size1.height - size2.height > 0) return 1;
        else if (size1.height - size2.height < 0) return -1;
        else return 0;
    }
}

int compare_rects(ei_rect_t rect1, ei_rect_t rect2)
{
    int res1 = compare_points(rect1.top_left, rect2.top_left);
    if (res1 > 0) return 1;
    else if (res1 < 0) return -1;
    else {
        int res2 = compare_sizes(rect1.size, rect2.size);
        if (res2 > 0) return 1;
        else if (res2 < 0) return -1;
        else return 0;
    }
}

void free_linked_point(ei_linked_point_t *first_element)
{
    ei_linked_point_t *current = first_element;
    ei_linked_point_t *next = NULL;
    while (current != NULL) {
        next = current->next;
        free(current);
        current = next;
    }
}

int min(int a, int b)
{
    return a < b ? a : b;
}

int max(int a, int b)
{
    return a > b ? a : b;
}

int minus(int a, int b)
{
    return a > b ? a - b : 0;
}

ei_rect_t ei_rect_intersect(ei_rect_t* rect1, const ei_rect_t* rect2)
{
    int xmin, xmax, ymin, ymax, wxmax, wxmin, hymax, hymin;
    if (rect1->top_left.x > rect2->top_left.x) {
        xmax = rect1->top_left.x;
        xmin = rect2->top_left.x;
        wxmax = rect1->size.width;
        wxmin = rect2->size.width;
    }
    else {
        xmax = rect2->top_left.x;
        xmin = rect1->top_left.x;
        wxmax = rect2->size.width;
        wxmin = rect1->size.width;
    }
    if (rect1->top_left.y > rect2->top_left.y) {
        ymax = rect1->top_left.y;
        ymin = rect2->top_left.y;
        hymax = rect1->size.height;
        hymin = rect2->size.height;
    }
    else {
        ymax = rect2->top_left.y;
        ymin = rect1->top_left.y;
        hymax = rect2->size.height;
        hymin = rect1->size.height;
    }
    int w = min(minus(xmin + wxmin, xmax), wxmax);
    int h = min(minus(ymin + hymin, ymax), hymax);
    ei_rect_t rec = {
        {xmax, ymax},
        {w, h}};
    return rec;
}



