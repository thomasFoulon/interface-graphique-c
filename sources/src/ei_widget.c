#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ei_application.h"
#include "ei_draw.h"
#include "ei_event.h"
#include "ei_offscreen.h"
#include "ei_utils.h"
#include "ei_widgetclasses.h"
#include "ei_widgetfunctions.h"
#include "ei_tools.h"


ei_color_t current_pick_color = {0, 0, 0, 255};

static int          k_default_toplevel_close_border     = 1;
static int          k_default_toplevel_close_corner     = 7;
static ei_anchor_t  k_default_toplevel_close_anchor     = ei_anc_northwest;
static int          k_default_toplevel_close_abs_width  = 13;
static int          k_default_toplevel_close_abs_x      = 20;
static int          k_default_toplevel_close_abs_y      = -20;
static float        k_default_toplevel_close_rel_x      = 0.0;
static float        k_default_toplevel_close_rel_y      = 0.0;
static int          k_default_toplevel_resize_border    = 0;
static int          k_default_toplevel_resize_corner    = 0;
static ei_anchor_t  k_default_toplevel_resize_anchor    = ei_anc_southeast;
static int          k_default_toplevel_resize_abs_width = 13;
static int          k_default_toplevel_resize_abs_x     = 0;
static int          k_default_toplevel_resize_abs_y     = 0;
static float        k_default_toplevel_resize_rel_x     = 1.0;
static float        k_default_toplevel_resize_rel_y     = 1.0;

/**
 * \brief Linked list of callback function to call when the widget is destroyed.
 */
typedef struct widget_callback_destroy_t {
    ei_widget_t*                        widget;
    ei_callback_t                       callback;
    void*                               user_param;
    struct widget_callback_destroy_t*   next;
} widget_callback_destroy_t;

widget_callback_destroy_t *head_destroy_cb = NULL;


/**
 * \brief   Update next pick id and return it.
 * 
 * @return  A pick id made from pick color.
 */
uint32_t next_pick_id()
{
    if (current_pick_color.red < 255) ++current_pick_color.red;
    else if (current_pick_color.green < 255) ++current_pick_color.green;
    else ++current_pick_color.blue;
    return ei_map_rgba(ei_offscreen_get(), &current_pick_color);
}

ei_widget_t* ei_widget_create(ei_widgetclass_name_t class_name,
                              ei_widget_t* parent)
{
    ei_widgetclass_t *class = ei_widgetclass_from_name(class_name);
    ei_widget_t *new_widget = class->allocfunc();
    new_widget->wclass = class;
    new_widget->wclass->setdefaultsfunc(new_widget);
    new_widget->parent = parent;
    new_widget->pick_id = next_pick_id();
    new_widget->pick_color = malloc(sizeof(ei_color_t));
    *new_widget->pick_color = current_pick_color;
    if (parent != NULL) {
        ei_widget_t *current_brother = parent->children_head;
        if (current_brother == NULL) {
            // The new widget is the first child of his parent
            parent->children_head = new_widget;
            parent->children_tail = new_widget;
        }
        else {
            while (current_brother->next_sibling != NULL) {
                current_brother = current_brother->next_sibling;
            }
            current_brother->next_sibling = new_widget;
        }
        parent->children_tail = new_widget;
    }
    new_widget->next_sibling = NULL;
    new_widget->children_head = NULL;
    new_widget->children_tail = NULL;
    return new_widget;
}

ei_widget_t *remove_from_parent(ei_widget_t* widget)
{
    if (widget->parent == NULL) return NULL;
    ei_widget_t* current_child = widget->parent->children_head;
    ei_widget_t* previous_child = NULL;
    while (current_child != NULL) {
        if (widget == current_child) {
            if (previous_child == NULL) {
                widget->parent->children_head = current_child->next_sibling;
            }
            else {
                previous_child->next_sibling = current_child->next_sibling;
            }
            if (current_child == widget->parent->children_tail) {
                widget->parent->children_tail = previous_child;
            }
            current_child->next_sibling = NULL;
            return current_child;
        }
        previous_child = current_child;
        current_child = current_child->next_sibling;
    }
    return NULL;
}

/**
 * \brief   Registrer the a callback function and its widget in a global linked list.
 *          When the widget is destroyed, this callback have to be called.
 * 
 * @param       widget      The destroyed widget.
 * @param       callback    The callback of the widget to call when the widget is destroyed.
 * @param       user_param  Some param for the callback.
 */
void ei_widget_set_destroy_cb(ei_widget_t *widget,
                              ei_callback_t callback,
                              void *user_param)
{
    if (head_destroy_cb == NULL) {
        head_destroy_cb = malloc(sizeof(widget_callback_destroy_t));
        head_destroy_cb->widget = widget;
        if (callback != NULL) head_destroy_cb->callback = callback;
        else head_destroy_cb->callback = NULL;
        head_destroy_cb->user_param = user_param;
        head_destroy_cb->next = NULL;
    }
    else {
        widget_callback_destroy_t *new = malloc(sizeof(widget_callback_destroy_t));
        new->widget = widget;
        if (callback != NULL) new->callback = callback;
        else new->callback = NULL;
        new->user_param = user_param;
        new->next = head_destroy_cb;
        head_destroy_cb = new;
    }
}

/**
 * \brief   Remove the widget from the list of callback destroy structure.
 * 
 * @param       widget  The widget to remove.
 * 
 * @return  The widget so we can call its callback destroy function.
 */
widget_callback_destroy_t *remove_from_destroy_cb_list(ei_widget_t* widget)
{
    if (head_destroy_cb == NULL) return NULL;
    widget_callback_destroy_t* current = head_destroy_cb;
    widget_callback_destroy_t* previous = NULL;
    while (current != NULL) {
        if (widget == current->widget) {
            if (previous == NULL) {
                head_destroy_cb = current->next;
            }
            else {
                previous->next = current->next;
                
            }
            current->next = NULL;
            return current;
        }
        previous = current;
        current = current->next;
    }
    return NULL;
}

void ei_widget_destroy(ei_widget_t* widget)
{
    widget_callback_destroy_t *destroy_cb = remove_from_destroy_cb_list(widget);
    if (destroy_cb != NULL && destroy_cb->callback != NULL)
        destroy_cb->callback(widget, NULL, destroy_cb->user_param);
    if (destroy_cb != NULL) free(destroy_cb);
    if (widget->pick_color != NULL) free(widget->pick_color);
    while(widget->children_head != NULL){
        ei_widget_destroy(remove_from_parent(widget->children_head));
    }
    ei_placer_forget(widget);
    if (widget->content_rect != &widget->screen_location)
        free(widget->content_rect);
    widget->wclass->releasefunc(widget);
    remove_from_parent(widget);
    ei_app_invalidate_rect(&widget->screen_location);
    free(widget);
}

/**
 * \brief   Search for a widget by its pick id in the list of children of an initial widget.
 *          This initial widget is also incluse in the search.
 * 
 * @param       widget      First widget of search hierarchy.
 * @param       pick_id     Pick id of the wanted widget.
 * 
 * @return  The widget associated to the pick id.
 */
ei_widget_t *get_widget_by_pick_id(ei_widget_t *widget, uint32_t pick_id)
{
    if (widget->pick_id == pick_id) return widget;
    ei_widget_t *current_child = widget->children_head, *res;
    while (current_child != NULL) {
        if (strcmp(current_child->wclass->name, CLASS_NAME_TOP_LEVEL) == 0) {
            ei_top_level_widget_t *top_level = (ei_top_level_widget_t*) current_child;
            if (top_level->button_close != NULL && top_level->button_close->pick_id == pick_id)
                return top_level->button_close;
            if (top_level->resize != NULL && top_level->resize->pick_id == pick_id)
                return top_level->resize;
        }
        res = get_widget_by_pick_id(current_child, pick_id);
        if (res != NULL) return res;
        current_child = current_child->next_sibling;
    }
    return NULL;
}

ei_widget_t* ei_widget_pick(ei_point_t* where)
{
    uint32_t *p = (uint32_t *) hw_surface_get_buffer(ei_offscreen_get());
    ei_size_t offscreen_size = hw_surface_get_size(ei_offscreen_get());
    uint32_t pixel_color = *(p + where->y * offscreen_size.width + where->x);
    return get_widget_by_pick_id(ei_app_root_widget(), pixel_color);
}

void ei_frame_configure(ei_widget_t* widget,
                        ei_size_t* requested_size,
                        const ei_color_t* color,
                        int* border_width,
                        ei_relief_t* relief,
                        char** text,
                        ei_font_t* text_font,
                        ei_color_t* text_color,
                        ei_anchor_t* text_anchor,
                        ei_surface_t* img,
                        ei_rect_t** img_rect,
                        ei_anchor_t* img_anchor)
{
    ei_widget_t* default_widget = ei_widget_create(CLASS_NAME_FRAME, NULL);
    ei_frame_widget_t *default_frame = (ei_frame_widget_t*) default_widget;
    ei_setdefaults_frame(default_widget);
    ei_frame_widget_t *frame = (ei_frame_widget_t *) widget;
    
    if (color != NULL) frame->color = *color;

    if (border_width != NULL) frame->border_width = *border_width;
    else if (frame->border_width == -1) frame->border_width = default_frame->border_width;

    if (relief != NULL) frame->relief = *relief;

    if (text != NULL) {
        if (frame->text != NULL) free(frame->text);
        frame->text = malloc(sizeof(char) * (strlen(*text) + 1));
        strcpy(frame->text, *text);
    }

    if (text_font != NULL) frame->text_font = *text_font;

    if (text_color != NULL) frame->text_color = *text_color;

    if (text_anchor != NULL) frame->text_anchor = *text_anchor;
    else if (frame->text_anchor == ei_anc_none) frame->text_anchor = default_frame->text_anchor;

    if (img != NULL) frame->img = *img;

    if (img_rect != NULL) {
        if (frame->img_rect != NULL) free(frame->img_rect);
        frame->img_rect = malloc(sizeof(ei_rect_t));
        *frame->img_rect = **img_rect;
    }

    if (img_anchor != NULL) frame->img_anchor = *img_anchor;

    if (requested_size != NULL) widget->requested_size = *requested_size;
    else if (widget->requested_size.width == -1 && widget->requested_size.height == -1) {
        int width_text = 0, height_text = 0, width_img = 0, height_img = 0;
        if (frame->text != NULL) hw_text_compute_size(frame->text, frame->text_font, &width_text, &height_text);
        if (frame->img_rect != NULL) {
            width_img = frame->img_rect->size.width;
            height_img = frame->img_rect->size.height;
        }
        widget->requested_size.width = (width_text < width_img) ? width_img : width_text + frame->border_width;
        widget->requested_size.height = (height_text < height_img) ? height_img : height_text + frame->border_width;
    }

    ei_widget_destroy(default_widget);
    // Style has been modified and the widget need to be redrawn
    ei_app_invalidate_rect(&widget->screen_location);
}

void ei_button_configure(ei_widget_t* widget,
                         ei_size_t* requested_size,
                         const ei_color_t* color,
                         int* border_width,
                         int* corner_radius,
                         ei_relief_t* relief,
                         char** text,
                         ei_font_t* text_font,
                         ei_color_t* text_color,
                         ei_anchor_t* text_anchor,
                         ei_surface_t* img,
                         ei_rect_t** img_rect,
                         ei_anchor_t* img_anchor,
                         ei_callback_t* callback,
                         void** user_param)
{
    ei_widget_t *default_widget = ei_widget_create(CLASS_NAME_BUTTON, NULL);
    ei_button_widget_t *default_button = (ei_button_widget_t*) default_widget;
    ei_setdefaults_button(default_widget);
    ei_button_widget_t *button = (ei_button_widget_t *) widget;
    
    if (color != NULL) button->color = *color;

    if (border_width != NULL) button->border_width = *border_width;
    else if (button->border_width == -1) button->border_width = default_button->border_width;

    if (corner_radius != NULL) button->corner_radius = *corner_radius;
    else if (button->corner_radius == -1) button->corner_radius = default_button->corner_radius;

    if (relief != NULL) button->relief = *relief;

    if (text != NULL) {
        if (button->text != NULL) {
            free(button->text);
        }
        button->text = malloc(sizeof(char) * (strlen(*text) + 1));
        strcpy(button->text, *text);
    }

    if (text_font != NULL) button->text_font = *text_font;

    if (text_color != NULL) button->text_color = *text_color;

    if (text_anchor != NULL) button->text_anchor = *text_anchor;
    else if (button->text_anchor == ei_anc_none) button->text_anchor = default_button->text_anchor;

    if (img != NULL) button->img = *img;

    if (img_rect != NULL) {
        if (button->img_rect != NULL) {
            free(button->img_rect);
        }
        button->img_rect = malloc(sizeof(ei_rect_t));
        *button->img_rect = **img_rect;
    }

    if (img_anchor != NULL) button->img_anchor = *img_anchor;

    if (callback != NULL) button->callback = *callback;

    if (user_param != NULL) button->user_param = *user_param;

    if (requested_size != NULL) widget->requested_size = *requested_size;
    else if (widget->requested_size.width == -1 && widget->requested_size.height == -1) {
        int width_text = 0, height_text = 0, width_img = 0, height_img = 0;
        if (button->text != NULL) hw_text_compute_size(button->text, button->text_font, &width_text, &height_text);
        if (button->img_rect != NULL) {
            width_img = button->img_rect->size.width;
            height_img = button->img_rect->size.height;
        }
        widget->requested_size.width = (width_text < width_img) ? width_img : width_text + button->border_width;
        widget->requested_size.height = (height_text < height_img) ? height_img : height_text + button->border_width;
    }

    ei_widget_destroy(default_widget);
    // Style has been modified and the widget need to be redrawn
    ei_app_invalidate_rect(&widget->screen_location);
}

/**
 * \brief   Extern callback function defined by library for top level's closing button.
 * 
 * @param       widget      The close button.
 * @param       event       Trigger event.
 * @param       user_param  Top level to destroy.
 */
void close_top_level(ei_widget_t *widget, ei_event_t *event, void *user_param)
{
    ei_widget_t *top_level = (ei_widget_t *) user_param;
    ei_widget_destroy(top_level);
}

/**
 * @brief	Configures the attributes of widgets of the class "resize".
 *
 * @param	widget, requested_size, color, border_width, relief,
 *		text, text_font, text_color, text_anchor,
 *		img, img_rect, img_anchor
 *				See the parameter definition of \ref ei_frame_configure. The only
 *				difference is that relief defaults to \ref ei_relief_raised
 *				and border_width defaults to \ref k_default_button_border_width.
 * @param	corner_radius, callback, user_param
 *                              See the parameter definition of \ref ei_button_configure.
 */
void ei_resize_configure(ei_widget_t* widget,
                         ei_size_t* requested_size,
                         const ei_color_t* color,
                         int* border_width,
                         int* corner_radius,
                         ei_relief_t* relief,
                         char** text,
                         ei_font_t* text_font,
                         ei_color_t* text_color,
                         ei_anchor_t* text_anchor,
                         ei_surface_t* img,
                         ei_rect_t** img_rect,
                         ei_anchor_t* img_anchor,
                         ei_callback_t* callback,
                         void** user_param)
{
    ei_widget_t *default_widget = ei_widget_create(CLASS_NAME_RESIZE, NULL);
    ei_resize_widget_t *default_resize = (ei_resize_widget_t*) default_widget;
    ei_setdefaults_resize(default_widget);
    ei_resize_widget_t *resize = (ei_resize_widget_t *) widget;
    
    if (color != NULL) resize->color = *color;

    if (border_width != NULL) resize->border_width = *border_width;
    else if (resize->border_width == -1) resize->border_width = default_resize->border_width;

    if (corner_radius != NULL) resize->corner_radius = *corner_radius;
    else if (resize->corner_radius == -1) resize->corner_radius = default_resize->corner_radius;

    if (relief != NULL) resize->relief = *relief;

    if (text != NULL) {
        if (resize->text != NULL) free(resize->text);
        resize->text = malloc(sizeof(char) * (strlen(*text) + 1));
        strcpy(resize->text, *text);
    }

    if (text_font != NULL) resize->text_font = *text_font;

    if (text_color != NULL) resize->text_color = *text_color;

    if (text_anchor != NULL) resize->text_anchor = *text_anchor;
    else if (resize->text_anchor == ei_anc_none) resize->text_anchor = default_resize->text_anchor;

    if (img != NULL) resize->img = *img;

    if (img_rect != NULL) {
        if (resize->img_rect != NULL) free(resize->img_rect);
        resize->img_rect = malloc(sizeof(ei_rect_t));
        *resize->img_rect = **img_rect;
    }

    if (img_anchor != NULL) resize->img_anchor = *img_anchor;

    if (callback != NULL) resize->callback = *callback;

    if (user_param != NULL) resize->user_param = *user_param;

    if (requested_size != NULL) widget->requested_size = *requested_size;
    else if (widget->requested_size.width == -1 && widget->requested_size.height == -1) {
        int width_text = 0, height_text = 0, width_img = 0, height_img = 0;
        if (resize->text != NULL) hw_text_compute_size(resize->text, resize->text_font, &width_text, &height_text);
        if (resize->img_rect != NULL) {
            width_img = resize->img_rect->size.width;
            height_img = resize->img_rect->size.height;
        }
        widget->requested_size.width = (width_text < width_img) ? width_img : width_text + resize->border_width;
        widget->requested_size.height = (height_text < height_img) ? height_img : height_text + resize->border_width;
    }

    ei_widget_destroy(default_widget);
    // Style has been modified and the widget need to be redrawn
    ei_app_invalidate_rect(&widget->screen_location);
}

void ei_toplevel_configure(ei_widget_t* widget,
                           ei_size_t* requested_size,
                           ei_color_t* color,
                           int* border_width,
                           char** title,
                           ei_bool_t* closable,
                           ei_axis_set_t* resizable,
                           ei_size_t** min_size)
{
    ei_widget_t *default_widget = ei_widget_create(CLASS_NAME_TOP_LEVEL, NULL);
    ei_top_level_widget_t *default_top_level = (ei_top_level_widget_t*) default_widget;
    ei_setdefaults_top_level(default_widget);
    ei_top_level_widget_t *top_level = (ei_top_level_widget_t *) widget;
    
    if (requested_size != NULL) top_level->widget.requested_size = ei_size(requested_size->width, requested_size->height + k_default_toplevel_titlebar_height);
    else if(top_level->widget.requested_size.width == -1 && top_level->widget.requested_size.height == -1)
        top_level->widget.requested_size = default_top_level->widget.requested_size;

    if (color != NULL) top_level->color = *color;

    if (border_width != NULL) top_level->border_width = *border_width;

    if (title != NULL) {
        if (top_level->title != NULL) free(top_level->title);
        top_level->title = malloc(sizeof(char) * (strlen(*title) + 1));
        strcpy(top_level->title, *title);
    }
    else if (top_level->title == NULL) {
        top_level->title = malloc(sizeof(char) * (strlen(default_top_level->title) + 1));
        strcpy(top_level->title, default_top_level->title);
    }

    if (closable != NULL) {
        top_level->closable = *closable;
        if (*closable == EI_TRUE) {
            // Close button
            top_level->button_close = ei_widget_create(CLASS_NAME_BUTTON, NULL);
            top_level->button_close->parent = widget;

            ei_callback_t close = close_top_level;

            ei_button_configure(top_level->button_close, NULL, &k_default_toplevel_closebutton_color,
                    &k_default_toplevel_close_border,
                    &k_default_toplevel_close_corner,
                    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                    &close, (void**) &widget);

            ei_place(top_level->button_close, &k_default_toplevel_close_anchor,
                    &k_default_toplevel_close_abs_x,
                    &k_default_toplevel_close_abs_y,
                    &k_default_toplevel_close_abs_width,
                    &k_default_toplevel_close_abs_width,
                    &k_default_toplevel_close_rel_x,
                    &k_default_toplevel_close_rel_y, NULL, NULL);
        }
    }

    if (resizable != NULL) {
        top_level->resizable = *resizable;

        if (top_level->resizable != ei_axis_none) {
            // Resize button
            top_level->resize = ei_widget_create(CLASS_NAME_RESIZE, NULL);
            top_level->resize->parent = widget;

            ei_resize_configure(top_level->resize, NULL, &k_default_toplevel_titlebar_color,
                    &k_default_toplevel_resize_border,
                    &k_default_toplevel_resize_corner,
                    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                    NULL, NULL);

            ei_place(top_level->resize, &k_default_toplevel_resize_anchor,
                    &k_default_toplevel_resize_abs_x,
                    &k_default_toplevel_resize_abs_y,
                    &k_default_toplevel_resize_abs_width,
                    &k_default_toplevel_resize_abs_width,
                    &k_default_toplevel_resize_rel_x,
                    &k_default_toplevel_resize_rel_y, NULL, NULL);
        }
    }

    if (min_size != NULL) {
        top_level->min_size = malloc(sizeof(ei_size_t));
        *(top_level->min_size) = **min_size;
    }
    else if (top_level->min_size == NULL) {
        top_level->min_size = malloc(sizeof(ei_size_t));
        *top_level->min_size = *default_top_level->min_size;
    }

    ei_widget_destroy(default_widget);
    // Style has been modified and the widget need to be redrawn
    ei_app_invalidate_rect(&widget->screen_location);
}
