#include "ei_offscreen.h"


ei_surface_t offscreen;



void ei_offscreen_init(ei_surface_t main_window, ei_size_t *size)
{
    offscreen = hw_surface_create(main_window, size, EI_FALSE);
}

ei_surface_t ei_offscreen_get()
{
    return offscreen;
}