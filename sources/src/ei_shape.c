#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <stdio.h>

#include "ei_shape.h"
#include "ei_utils.h"
#include "ei_widgetfunctions.h"

#define PI 3.14159265358979323846



ei_linked_point_t *ei_get_arc(ei_point_t center, int radius, ei_bool_t octants[8])
{
    int x = 0, y = radius, m = 5 - 4 * radius;
    ei_linked_point_t * current[8] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
    *head[8] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
    *tail[8] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
    *new = NULL, *previous = NULL, *first = NULL, *last = NULL;
    while (x <= y) {
        if (first == NULL && last == NULL) {
            // First passage in the loop
            for (uint8_t i = 0; i < 8; ++i) {
                if (octants[i]) {
                    current[i] = malloc(sizeof(ei_linked_point_t));
                    if (i % 2 == 0) {
                        head[i] = current[i];
                        if (first == NULL) first = head[i];
                    }
                    else {
                        tail[i] = current[i];
                        last = tail[i];
                    }
                }
            }
        }
        else {
            for (uint8_t i = 0; i < 8; ++i) {
                if (octants[i]) {
                    if (i % 2 == 0) {
                        // Normal way (insertion in tail)
                        current[i]->next = malloc(sizeof(ei_linked_point_t));
                        current[i] = current[i]->next;
                    }
                    else {
                        // Inverted way (insertion in head)
                        new = malloc(sizeof(ei_linked_point_t));
                        new->next = current[i];
                        current[i] = new;
                    }
                }
            }
        }
        if (octants[6]) current[6]->point = ei_point(x + center.x, y + center.y);
        if (octants[7]) current[7]->point = ei_point(y + center.x, x + center.y);
        if (octants[5]) current[5]->point = ei_point(-x + center.x, y + center.y);
        if (octants[4]) current[4]->point = ei_point(-y + center.x, x + center.y);
        if (octants[1]) current[1]->point = ei_point(x + center.x, -y + center.y);
        if (octants[0]) current[0]->point = ei_point(y + center.x, -x + center.y);
        if (octants[2]) current[2]->point = ei_point(-x + center.x, -y + center.y);
        if (octants[3]) current[3]->point = ei_point(-y + center.x, -x + center.y);
        if (m > 0) {
            --y;
            m -= 8 * y;
        }
        ++x;
        m += 8 * x + 4;
    }
    // Link the octants between them
    for (uint8_t i = 0; i < 8; ++i) {
        if (octants[i]) {
            if (previous == NULL) {
                // First passage in the loop
                if (i % 2 == 0) {
                    previous = current[i];
                    first = head[i];
                }
                else {
                    previous = tail[i];
                    first = current[i];
                }
            }
            else {
                if (i % 2 == 0) {
                    previous->next = head[i];
                    previous = current[i];
                }
                else {
                    previous->next = current[i];
                    previous = tail[i];
                }
            }
        }
    }
    if (previous != NULL) previous->next = NULL;
    return first;
}

/**
 * \brief	Set the elements (of type \ref ei_point_t) in an array.
 *
 * @param list            The list the function puts the elements in.
 * @param num             The number of elements to put in the array.
 * @param ...             The elements (of type \ref ei_point_t) to put in the array.
 */
void set_ei_point_elements(ei_point_t *list, int num, ...)
{
    va_list valist;
    va_start(valist, num);
    for (uint8_t i = 0; i < num; ++i) {
        list[i] = va_arg(valist, ei_point_t);
    }
    va_end(valist);
}

/**
 * \brief	Set the elements (of type \ref ei_bool_t) in an array.
 *
 * @param list            The list the function puts the elements in.
 * @param num             The number of elements to put in the array.
 * @param ...             The elements (of type ei_bool_t) to put in the array.
 */
void set_ei_bool_elements(ei_bool_t *list, int num, ...)
{
    va_list valist;
    va_start(valist, num);
    for (uint8_t i = 0; i < num; ++i) {
        list[i] = va_arg(valist, ei_bool_t);
    }
    va_end(valist);
}

/**
 * \brief	Determine the configuration of the rounded frame according to the part to draw.
 *
 * @param list            The points list of the rounded frame vertices to draw in order.
 * @param octants         The octants of the arcs.
 * @param rect            The rectangle which is the base of the rounded frame.
 * @param part            The part of the rounded frame to draw.
 */
void get_rounded_frame_config(ei_point_t *centers, ei_bool_t **octants, ei_rect_t rect, ei_shape_part_t part)
{
    ei_point_t p0 = rect.top_left,
            p1 = ei_point(rect.top_left.x, rect.top_left.y + rect.size.height),
            p2 = ei_point(rect.top_left.x + rect.size.width, rect.top_left.y + rect.size.height),
            p3 = ei_point(rect.top_left.x + rect.size.width, rect.top_left.y);
    if (part == BOTTOM) {
        set_ei_point_elements(centers, 3, p1, p2, p3);
        set_ei_bool_elements(octants[0], 8, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_TRUE, EI_FALSE, EI_FALSE);
        set_ei_bool_elements(octants[1], 8, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_TRUE, EI_TRUE);
        set_ei_bool_elements(octants[2], 8, EI_TRUE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE);
    }
    else if (part == TOP) {
        set_ei_point_elements(centers, 3, p3, p0, p1);
        set_ei_bool_elements(octants[0], 8, EI_FALSE, EI_TRUE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE);
        set_ei_bool_elements(octants[1], 8, EI_FALSE, EI_FALSE, EI_TRUE, EI_TRUE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE);
        set_ei_bool_elements(octants[2], 8, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_TRUE, EI_FALSE, EI_FALSE, EI_FALSE);
    }
    else {
        set_ei_point_elements(centers, 4, p3, p0, p1, p2);
        set_ei_bool_elements(octants[0], 8, EI_TRUE, EI_TRUE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE);
        set_ei_bool_elements(octants[1], 8, EI_FALSE, EI_FALSE, EI_TRUE, EI_TRUE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE);
        set_ei_bool_elements(octants[2], 8, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_TRUE, EI_TRUE, EI_FALSE, EI_FALSE);
        set_ei_bool_elements(octants[3], 8, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_TRUE, EI_TRUE);
    }
}

ei_linked_point_t *ei_get_rounded_frame(ei_rect_t rect, int radius, ei_shape_part_t part)
{
    uint8_t nb_arcs = (part == ALL) ? 4 : 3;
    ei_point_t centers[nb_arcs];
    // Malloc to avoid the warning when passing as argument to the function
    ei_bool_t **octants = malloc(nb_arcs * sizeof(ei_bool_t *));
    for (int i = 0; i < nb_arcs; ++i) {
        octants[i] = malloc(8 * sizeof(ei_bool_t));
    }
    get_rounded_frame_config(centers, octants, rect, part);
    ei_linked_point_t * arcs[nb_arcs];
    // Get the arcs points of the rounded frame
    for (uint8_t i = 0; i < nb_arcs; ++i) {
        arcs[i] = ei_get_arc(centers[i], radius, octants[i]);
    }
    for (int i = 0; i < nb_arcs; ++i) {
        free(octants[i]);
    }
    free(octants);
    ei_linked_point_t *rounded_frame = arcs[0];
    ei_linked_point_t *current = rounded_frame;
    // Link the points of the arcs between them
    for (uint8_t i = 0; i < nb_arcs; ++i) {
        while (current->next != NULL) {
            current = current->next;
        }
        if (i < nb_arcs - 1) current->next = arcs[i + 1];
    }
    ei_point_t pa = ei_point(rect.top_left.x + rect.size.width / 3, rect.top_left.y + rect.size.height / 2),
            pb = ei_point(rect.top_left.x + 2 * rect.size.width / 3, rect.top_left.y + rect.size.height / 2);
    if (part != ALL) {
        current->next = malloc(sizeof(ei_linked_point_t));
        current = current->next;
        current->point = (part == BOTTOM) ? pb : pa;
        current->next = malloc(sizeof(ei_linked_point_t));
        current = current->next;
        current->point = (part == BOTTOM) ? pa : pb;
    }
    current->next = NULL;
    return rounded_frame;
}

ei_linked_point_t *ei_get_rectangle_frame(ei_rect_t *rect)
{
    ei_point_t top_right = {rect->top_left.x + rect->size.width, rect->top_left.y};
    ei_point_t bot_left = {rect->top_left.x, rect->top_left.y + rect->size.height};
    ei_point_t bot_right = {rect->top_left.x + rect->size.width, rect->top_left.y + rect->size.height};

    ei_linked_point_t *d = malloc(sizeof(ei_linked_point_t));
    d->point = bot_left;
    d->next = NULL;

    ei_linked_point_t *c = malloc(sizeof(ei_linked_point_t));
    c->point = bot_right;
    c->next = d;

    ei_linked_point_t *b = malloc(sizeof(ei_linked_point_t));
    b->point = top_right;
    b->next = c;

    ei_linked_point_t *a = malloc(sizeof(ei_linked_point_t));
    a->point = rect->top_left;
    a->next = b;

    return a;
}

ei_linked_point_t *ei_get_bar_title(ei_rect_t *rect)
{
    ei_point_t center_left = ei_point_add(rect->top_left, ei_point(rect->size.height, rect->size.height));
    ei_bool_t octants_left[8] = {EI_FALSE, EI_FALSE, EI_TRUE, EI_TRUE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE};
    ei_linked_point_t *left_arc = ei_get_arc(center_left, rect->size.height, octants_left);

    ei_point_t center_right = ei_point_add(rect->top_left, ei_point(rect->size.width - rect->size.height, rect->size.height));
    ei_bool_t octants_right[8] = {EI_TRUE, EI_TRUE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE};
    ei_linked_point_t *right_arc = ei_get_arc(center_right, rect->size.height, octants_right);

    // Link both list
    ei_linked_point_t *current = right_arc;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = left_arc;

    return right_arc;
}
