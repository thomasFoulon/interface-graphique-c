#include <stdlib.h>
#include <malloc.h>

#include "ei_widget.h"
#include "ei_placer.h"
#include "ei_utils.h"
#include "ei_application.h"
#include "ei_tools.h"



void ei_place(struct ei_widget_t* widget,
              ei_anchor_t* anchor,
              int* x,
              int* y,
              int* width,
              int* height,
              float* rel_x,
              float* rel_y,
              float* rel_width,
              float* rel_height)
{
    if (widget->placer_params == NULL) {
        widget->placer_params = malloc(sizeof(ei_placer_params_t));
        widget->placer_params->anchor = NULL;
        widget->placer_params->x = NULL;
        widget->placer_params->y = NULL;
        widget->placer_params->h = NULL;
        widget->placer_params->w = NULL;
        widget->placer_params->rx = NULL;
        widget->placer_params->ry = NULL;
        widget->placer_params->rw = NULL;
        widget->placer_params->rh = NULL;
    }

    if (anchor != NULL) {
        widget->placer_params->anchor_data = *anchor;
        widget->placer_params->anchor = &widget->placer_params->anchor_data;
    }
    if (x != NULL) {
        widget->placer_params->x_data = *x;
        widget->placer_params->x = &widget->placer_params->x_data;
    }
    if (y != NULL) {
        widget->placer_params->y_data = *y;
        widget->placer_params->y = &widget->placer_params->y_data;
    }
    if (width != NULL) {
        widget->placer_params->w_data = *width;
        widget->placer_params->w = &widget->placer_params->w_data;
    }
    if (height != NULL) {
        widget->placer_params->h_data = *height;
        widget->placer_params->h = &widget->placer_params->h_data;
    }
    if (rel_x != NULL) {
        widget->placer_params->rx_data = *rel_x;
        widget->placer_params->rx = &widget->placer_params->rx_data;
    }
    if (rel_y != NULL) {
        widget->placer_params->ry_data = *rel_y;
        widget->placer_params->ry = &widget->placer_params->ry_data;
    }
    if (rel_width != NULL) {
        widget->placer_params->rw_data = *rel_width;
        widget->placer_params->rw = &widget->placer_params->rw_data;
    }
    if (rel_height != NULL) {
        widget->placer_params->rh_data = *rel_height;
        widget->placer_params->rh = &widget->placer_params->rh_data;
    }
}

void ei_placer_run(struct ei_widget_t* widget)
{
    // In case of no placer params : do nothing
    if (widget->placer_params == NULL) return;
    
    ei_rect_t new_screen_location = widget->screen_location;

    // Width
    if(widget->placer_params->rw != NULL){
        int width = *widget->placer_params->rw * widget->parent->content_rect->size.width;
        if (widget->placer_params->w != NULL) width += *widget->placer_params->w;
        new_screen_location.size.width = width;
    }
    else if (widget->placer_params->w != NULL) new_screen_location.size.width = *widget->placer_params->w;
    else if (widget->requested_size.width != -1) new_screen_location.size.width = widget->requested_size.width;
    else new_screen_location.size.width = 0;

    // Height
    if(widget->placer_params->rh != NULL){
        int height = *widget->placer_params->rh * widget->parent->content_rect->size.height;
        if (widget->placer_params->h != NULL) height += *widget->placer_params->h;
        new_screen_location.size.height = height;
    }
    else if (widget->placer_params->h != NULL) new_screen_location.size.height = *widget->placer_params->h;
    else if (widget->requested_size.height != -1) new_screen_location.size.height = widget->requested_size.height;
    else new_screen_location.size.height = 0;

    // Positioning
    ei_anchor_t anchor;
    if (widget->placer_params->anchor != NULL) anchor = *widget->placer_params->anchor;
    else anchor = ei_anc_northwest;

    ei_point_t to_anchor;

    switch (anchor) {
    case ei_anc_center:
        to_anchor = ei_point(-new_screen_location.size.width / 2, -new_screen_location.size.height / 2);
        break;

    case ei_anc_north:
        to_anchor = ei_point(-new_screen_location.size.width / 2, 0);
        break;

    case ei_anc_northeast:
        to_anchor = ei_point(-new_screen_location.size.width, 0);
        break;

    case ei_anc_east:
        to_anchor = ei_point(-new_screen_location.size.width, -new_screen_location.size.height / 2);
        break;

    case ei_anc_southeast:
        to_anchor = ei_point(-new_screen_location.size.width, -new_screen_location.size.height);
        break;

    case ei_anc_south:
        to_anchor = ei_point(-new_screen_location.size.width / 2, -new_screen_location.size.height);
        break;

    case ei_anc_southwest:
        to_anchor = ei_point(0, -new_screen_location.size.height);
        break;

    case ei_anc_west:
        to_anchor = ei_point(0, -new_screen_location.size.height / 2);
        break;

    case ei_anc_northwest:
        to_anchor = ei_point(0, 0);
        break;

    default:
        exit(EXIT_FAILURE);
    }

    // x
    if(widget->parent != NULL) new_screen_location.top_left.x = widget->parent->content_rect->top_left.x;
    else new_screen_location.top_left.x = 0; // modif

    if(widget->placer_params->rx != NULL){
        int x = *widget->placer_params->rx * widget->parent->content_rect->size.width;
        if (widget->placer_params->x != NULL) x += *widget->placer_params->x;
        new_screen_location.top_left.x += x;
    }
    else if (widget->placer_params->x != NULL) new_screen_location.top_left.x += *widget->placer_params->x;


    // y
    if(widget->parent != NULL) new_screen_location.top_left.y = widget->parent->content_rect->top_left.y;
    else new_screen_location.top_left.y = 0;

    if(widget->placer_params->ry != NULL){
        int y = *widget->placer_params->ry * widget->parent->content_rect->size.height;
        if (widget->placer_params->y != NULL) y += *widget->placer_params->y;
        new_screen_location.top_left.y += y;
    }
    else if (widget->placer_params->y != NULL) new_screen_location.top_left.y += *widget->placer_params->y;

    // Take the anchor into account
    new_screen_location.top_left = ei_point_add(new_screen_location.top_left, to_anchor);
    
    // Notify to redraw the widget and its old location
    if(compare_rects(widget->screen_location, new_screen_location) != 0){
        widget->wclass->geomnotifyfunc(widget, new_screen_location);
    }
}

void ei_placer_forget(struct ei_widget_t* widget)
{
    if (widget->placer_params != NULL) {
        free(widget->placer_params);
        widget->placer_params = NULL;
    }
}
