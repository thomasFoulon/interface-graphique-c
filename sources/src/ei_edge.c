#include <stdlib.h>
#include <stdio.h>
#include "ei_types.h"
#include "ei_edge.h"
#include "hw_interface.h"



void push_front_edge_tc(ei_edge_t **tc,
                        const ei_double_linked_point_t *start,
                        const ei_double_linked_point_t *end)
{
    ei_edge_t *edge = malloc(sizeof(ei_edge_t));
    ei_point_t vertex_at_min_ordinate;
    ei_point_t vertex_at_max_ordinate;
    if (start->point.y >= end->point.y) {
        vertex_at_max_ordinate = start->point;
        vertex_at_min_ordinate = end->point;
    }
    else {
        vertex_at_max_ordinate = end->point;
        vertex_at_min_ordinate = start->point;
    }
    edge->y_max = vertex_at_max_ordinate.y;
    edge->x_ymin = vertex_at_min_ordinate.x;
    edge->x_ymax = vertex_at_max_ordinate.x;
    edge->dx = end->point.x - start->point.x;
    edge->dy = end->point.y - start->point.y;
    edge->error = 0;
    //    edge->reciprocal_slope = (double)(end->point.x - start->point.x) / (double)(end->point.y - start->point.y);
    edge->next = tc[start->point.y];
    tc[start->point.y] = edge;
}

void push_front_edge(ei_edge_t **list, ei_edge_t *edge)
{
    if (*list != NULL) {
        edge->next = *list;
    }
    *list = edge;
}

ei_edge_t* pop_edge(ei_edge_t **tca, ei_edge_t *edge)
{
    ei_edge_t *current = *tca;
    if (current == edge) {
        *tca = edge->next;
        return edge;
    }
    else {
        ei_edge_t *previous = current;
        current = current->next;
        while (current != NULL) {
            if (current == edge) {
                previous->next = current->next;
                return(edge);
            }
            else {
                previous = current;
                current = current->next;
            }
        }
    }
    return NULL;
}

void move_edges(ei_edge_t **list_destination, ei_edge_t **list_source)
{
    if (list_source == NULL) return;
    ei_edge_t *current = *list_source;
    ei_edge_t *next = NULL;
    while (current != NULL) {
        next = current->next;
        current->next = NULL;
        push_front_edge(list_destination, current);
        current = next;
    }
    *list_source = current;
}

/**
 * \brief   Update creation of TC for current scanline. 
 * 
 * @param       tc              The list of edge for a polygon.
 * @param       first_point     First point of the linked list of point of the polygon.
 * @param       indice_scanline Current scanline.
 */
void scanline(ei_edge_t **tc,
              ei_double_linked_point_t *first_point,
              uint32_t indice_scanline)
{
    // Go through every point where y = scanline
    ei_double_linked_point_t *current = first_point;
    do {
        if (current->point.y == indice_scanline) {
            // Check both points at which current point is linked
            // If current point is at minimum y, it is the point the most above
            // The edge begin at this point if we look from top to bottom
            // We add this point in TC
            // We don't add horizontal edges (no <=)
            if (current->point.y < current->previous->point.y) {
                push_front_edge_tc(tc, current, current->previous);
            }
            if (current->point.y < current->next->point.y) {
                push_front_edge_tc(tc, current, current->next);
            }
        }
        current = current->next;
    } while (current != first_point);
}

ei_edge_t** create_tc(const ei_linked_point_t* first_point, ei_size_t *size)
{
    // TC has same size than surface
    ei_edge_t **tc = calloc(size->height, sizeof(ei_edge_t *));
    // Create a double linked list from linked list
    if (first_point != NULL) {
        ei_double_linked_point_t *first_double_point = ei_create_double_circular_linked_list(first_point);
        // Surface scanline
        for (uint32_t i = 0; i < size->height; i++) {
            scanline(tc, first_double_point, i);
        }
        free_double_linked_point(first_double_point);
    }
    return tc;
}

ei_double_linked_point_t *ei_create_double_circular_linked_list(const ei_linked_point_t *first_point)
{
    // First element of double linked list
    ei_double_linked_point_t *first_dlp = malloc(sizeof(ei_double_linked_point_t));
    first_dlp->point = first_point->point;
    first_dlp->next = NULL;
    first_dlp->previous = NULL;
    // For next points
    ei_linked_point_t *current = first_point->next;
    ei_double_linked_point_t *previous = first_dlp;
    while (current != NULL) {
        ei_double_linked_point_t *dlp = malloc(sizeof(ei_double_linked_point_t));
        previous->next = dlp;
        dlp->point = current->point;
        dlp->next = NULL;
        dlp->previous = previous;
        current = current->next;
        previous = dlp;
    }
    // Chain last point to first point
    previous->next = first_dlp;
    first_dlp->previous = previous;
    return first_dlp;
}

void free_tc(ei_edge_t **tc, ei_size_t *size)
{
    for (uint32_t i = 0; i < size->height; i++) {
        if (tc[i] != NULL) {
            free_edge(tc[i]);
        }
    }
    free(tc);
}

void free_tca(ei_edge_t *tca)
{
    ei_edge_t *current = tca, *next;
    while (current != NULL) {
        next = current->next;
        free(current);
        current = next;
    }
}

void free_edge(ei_edge_t *first_egde)
{
    ei_edge_t *current = first_egde;
    ei_edge_t *next = NULL;
    while (current != NULL) {
        next = current->next;
        free(current);
        current = next;
    }
}

void free_double_linked_point(ei_double_linked_point_t *first_point)
{
    ei_double_linked_point_t *current = first_point->next;
    ei_double_linked_point_t *next = NULL;
    while (current != first_point) {
        next = current->next;
        free(current);
        current = next;
    }
    free(current);
}

/**
 * \brief   Merge list of TCA.
 * 
 * @param       tca     Linked list of edges to merge.
 * @param       tca2    Linked list of edges to merge.
 */
void fusion_tca(ei_edge_t *tca, ei_edge_t *tca2)
{
    if (tca2->x_ymin < tca->x_ymin) {
        ei_edge_t tmp = *tca2;
        *tca2 = *tca;
        *tca = tmp;
    }
    ei_edge_t *current1 = tca, *current2 = tca2, *next;
    while (current1->next != NULL && current2 != NULL) {
        if (current2->x_ymin < current1->next->x_ymin) {
            next = current1->next;
            current1->next = current2;
            current2 = next;
        }
        current1 = current1->next;
    }
    current1->next = current2;
}

void sort_tca(ei_edge_t *tca)
{
    ei_edge_t *tca2;
    ei_edge_t *current = tca;
    uint32_t size = 0, count = 0;
    // Determine the size of the TCA
    while (current != NULL) {
        ++size;
        current = current->next;
    }
    if (size == 1) {
        // Stop condition
        return;
    }
    // Else divide and rule.
    current = tca;
    while (count < size / 2 - 1) {
        current = current->next;
        ++count;
    }
    tca2 = current->next;
    current->next = NULL;
    sort_tca(tca);
    sort_tca(tca2);
    fusion_tca(tca, tca2);
}
