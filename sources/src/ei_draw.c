#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <math.h>

#include "ei_draw.h"
#include "ei_edge.h"
#include "ei_print.h"
#include "ei_tools.h"
#include "ei_types.h"
#include "ei_utils.h"
#include "hw_interface.h"

#define NO_TRANSPARENCY 255

/**
 * \brief   Clip a polygon.
 *          Uses Sutherland–Hodgman algorithm.
 * 
 * @param       poly_points     The verteces of a polygon.
 * @param       clipper         The rectangle within to clip, can be NULL.
 * 
 * @return      The verteces of the polygon clipped w.r.t. the clipper or poly_points if clipper is NULL.
 */
ei_linked_point_t *suth_hodg_clip(ei_linked_point_t *poly_points, const ei_rect_t *clipper);

/**
 * \brief   Set the ith color of color32 to color.
 *
 * @param       color32         The pointer to the pixel to set.
 * @param       i               The color indice.
 * @param       color           The value to set.
 */
void set_color(uint32_t *color32, int i, unsigned char color)
{
    *color32 |= color << (i << 3);
}

/**
 *  \brief  Return the ith color of color32.
 *
 *  @param      color32         The pointer to the pixel.
 *  @param      i               The color indice.
 *
 *  @return     The ith color of color32.
 */
unsigned char get_color(uint32_t *color32, int i)
{
    return(*color32 & (0xff << (i << 3))) >> (i << 3);
}

uint32_t ei_map_rgba(ei_surface_t surface, const ei_color_t* color)
{
    uint32_t color32 = 0;
    int ir, ig, ib, ia;
    hw_surface_get_channel_indices(surface, &ir, &ig, &ib, &ia);
    set_color(&color32, ir, color->red);
    set_color(&color32, ig, color->green);
    set_color(&color32, ib, color->blue);
    if (hw_surface_has_alpha(surface)) {
        set_color(&color32, ia, color->alpha);
    }
    return color32;
}

/**
 * \brief   Draws a pixel.
 *
 *          In this case, the pixel has no transparency.
 *
 * @param       surface     The surface where to draw the pixel. The surface must be *locked* by \ref hw_surface_lock.
 * @param       point       The pixel's coordinates.
 * @param       color       The pixel's color.
 * @param       clipper     If not NULL, the drawing is restricted within this rectangle.
 */
void draw_pixel(ei_surface_t surface,
                ei_point_t point,
                uint32_t color,
                const ei_rect_t* clipper)
{
        if (point.x >= 0 && point.y >= 0 && (clipper == NULL || (point.x >= clipper->top_left.x
                                && point.y >= clipper->top_left.y
                                && point.x < clipper->size.width + clipper->top_left.x
                                && point.y < clipper->size.height + clipper->top_left.y))) {
    uint32_t *p = (uint32_t*) hw_surface_get_buffer(surface);
    ei_size_t size = hw_surface_get_size(surface);
    uint32_t *actual_color = p + (size.width * point.y + point.x);
    //        uint32_t new_color = ei_map_rgba(surface, &color);

    *actual_color = color;
        }
}

/**
 * \brief   Draws a pixel.
 *
 *          In this case, the pixel has transparency.
 *
 * @param       surface     The surface where to draw the pixel. The surface must be *locked* by \ref hw_surface_lock.
 * @param       point       The pixel's coordinates.
 * @param       color       The pixel's color.
 * @param       clipper     If not NULL, the drawing is restricted within this rectangle.
 */
void draw_pixel_alpha(ei_surface_t surface,
                      ei_point_t point,
                      const ei_color_t* color,
                      const ei_rect_t* clipper)
{
        if (point.x >= 0 && point.y >= 0 && (clipper == NULL || (point.x >= clipper->top_left.x
                                && point.y >= clipper->top_left.y
                                && point.x < clipper->size.width + clipper->top_left.x
                                && point.y < clipper->size.height + clipper->top_left.y))) {
    uint32_t *p = (uint32_t*) hw_surface_get_buffer(surface);
    ei_size_t size = hw_surface_get_size(surface);
    uint32_t *actual_color = p + (size.width * point.y + point.x);

    int ir, ig, ib, ia;
    hw_surface_get_channel_indices(surface, &ir, &ig, &ib, &ia);

    unsigned char red = get_color(actual_color, ir);
    unsigned char green = get_color(actual_color, ig);
    unsigned char blue = get_color(actual_color, ib);
    unsigned char alpha = get_color(actual_color, ia);

    red = (red * (255 - color->alpha) + color->red * color->alpha) / 255;
    green = (green * (255 - color->alpha) + color->green * color->alpha) / 255;
    blue = (blue * (255 - color->alpha) + color->blue * color->alpha) / 255;

    const ei_color_t mixed = {red, green, blue, alpha};

    *actual_color = ei_map_rgba(surface, &mixed);
        }
}

/**
 * \brief   Draw a line between two points.
 *
 * @param       surface     Where to draw the line. The surface must be *locked* by \ref hw_surface_lock.
 * @param       start       The first end point.
 * @param       end         The second end point.
 * @param       color       The color used to draw the line.
 * @param       clipper     If not NULL, the drawing is restricted within this rectangle.
 */
void draw_line_between(ei_surface_t surface,
                       ei_point_t start,
                       ei_point_t end,
                       const ei_color_t* color,
                       const ei_rect_t* clipper)
{
    if (end.x >= start.x) {
        if (end.y >= start.y) {
            int dx = end.x - start.x;
            int dy = end.y - start.y;
            int e = 0;

            if (dx >= dy) {
                while (start.x < end.x) {
                    draw_pixel_alpha(surface, start, color, clipper);

                    start.x += 1;
                    e += dy;
                    if (e << 1 > dx) {
                        start.y += 1;
                        e -= dx;
                    }
                }
            }
            else {
                while (start.y < end.y) {
                    draw_pixel_alpha(surface, start, color, clipper);

                    start.y += 1;
                    e += dx;
                    if (e << 1 > dy) {
                        start.x += 1;
                        e -= dy;
                    }
                }
            }
        }
        else {
            int dx = end.x - start.x;
            int dy = start.y - end.y;
            int e = 0;

            if (dx >= dy) {
                while (start.x < end.x) {
                    draw_pixel_alpha(surface, start, color, clipper);

                    start.x += 1;
                    e += dy;
                    if (e << 1 > dx) {
                        start.y -= 1;
                        e -= dx;
                    }
                }
            }
            else {
                while (start.y > end.y) {
                    draw_pixel_alpha(surface, start, color, clipper);

                    start.y -= 1;
                    e += dx;
                    if (e << 1 > dy) {
                        start.x += 1;
                        e -= dy;
                    }
                }
            }
        }
    }
    else {
        if (end.y >= start.y) {
            int dx = start.x - end.x;
            int dy = end.y - start.y;
            int e = 0;

            if (dx >= dy) {
                while (start.x > end.x) {
                    draw_pixel_alpha(surface, start, color, clipper);

                    start.x -= 1;
                    e += dy;
                    if (e << 1 > dx) {
                        start.y += 1;
                        e -= dx;
                    }
                }
            }
            else {
                while (start.y < end.y) {
                    draw_pixel_alpha(surface, start, color, clipper);

                    start.y += 1;
                    e += dx;
                    if (e << 1 > dy) {
                        start.x -= 1;
                        e -= dy;
                    }
                }
            }
        }
        else {
            int dx = start.x - end.x;
            int dy = start.y - end.y;
            int e = 0;

            if (dx >= dy) {
                while (start.x > end.x) {
                    draw_pixel_alpha(surface, start, color, clipper);

                    start.x -= 1;
                    e += dy;
                    if (e << 1 > dx) {
                        start.y -= 1;
                        e -= dx;
                    }
                }
            }
            else {
                while (start.y > end.y) {
                    draw_pixel_alpha(surface, start, color, clipper);

                    start.y -= 1;
                    e += dx;
                    if (e << 1 > dy) {
                        start.x -= 1;
                        e -= dy;
                    }
                }
            }
        }
    }
}

void ei_draw_polyline(ei_surface_t surface,
                      const ei_linked_point_t* first_point,
                      const ei_color_t color,
                      const ei_rect_t* clipper)
{
    //    uint32_t col = ei_map_rgba(surface, &color);
    while (first_point && first_point->next) {
        draw_line_between(surface, first_point->point, first_point->next->point, &color, clipper);
        first_point = first_point->next;
    }
}

/**
 * \brief   Draw pixel of current scanline.
 * 
 * @param       surface         Where to draw the pixels.
 * @param       scanline        The current scanline.
 * @param       tca             List of edge to update during the scanline.
 * @param       ptr             Pointer toward the first pixel of the current scanline.
 * @param       color           Color to put on pixel.
 * @param       clipper         Clipper of drawing.
 * @param       no_transparency True if we want to draw without transparency.
 * @param       color32         Same color as color but already converted into int32.
 */
void draw_scanline(ei_surface_t surface,
                   uint32_t scanline, ei_edge_t *tca,
                   uint32_t* ptr,
                   const ei_color_t* color,
                   const ei_rect_t* clipper,
                   ei_bool_t no_transparency,
                   uint32_t color32)
{
    ei_size_t size = hw_surface_get_size(surface);

    // Pointer toward pixel adress
    uint32_t *buffer = (uint32_t*) hw_surface_get_buffer(surface);
    buffer += size.width * scanline;

    uint8_t parity = 0;
    ei_edge_t *current = tca;
    while (current->next != NULL) {
        if ((parity & 1) == 0) {
            // Even
            // Color pixel between current and next
            for (uint32_t i = current->x_ymin; i < current->next->x_ymin; i++) {
                //                ei_point_t point = {i, scanline};
                if (no_transparency) {
                    *(ptr + i) = color32;
                }
                else {
                    //                    draw_pixel_alpha(surface, point, color, clipper);
                    uint32_t *actual_color = ptr + i;

                    int ir, ig, ib, ia;
                    hw_surface_get_channel_indices(surface, &ir, &ig, &ib, &ia);

                    unsigned char red = get_color(actual_color, ir);
                    unsigned char green = get_color(actual_color, ig);
                    unsigned char blue = get_color(actual_color, ib);
                    unsigned char alpha = get_color(actual_color, ia);

                    red = (red * (255 - color->alpha) + color->red * color->alpha) / 255;
                    green = (green * (255 - color->alpha) + color->green * color->alpha) / 255;
                    blue = (blue * (255 - color->alpha) + color->blue * color->alpha) / 255;

                    const ei_color_t mixed = {red, green, blue, alpha};

                    *actual_color = ei_map_rgba(surface, &mixed);
                }
            }
            parity++;
        }
        else {
            parity--;
        }
        current = current->next;
    }
}

/**
 * \brief   Update the abscissa of edge (x_ymin) to be able to draw the next scanline.
 * 
 * @param       tca     Linked list of edge to update.
 */
void update_abscissa(ei_edge_t* tca)
{
    ei_edge_t *current = tca;
    // x_ymin increase of 0, 1 or more depending on the error
    while (current != NULL) {
        if (current->x_ymin <= current->x_ymax) {
            current->error += current->dx;
            while (current->error << 1 > current->dy) {
                current->x_ymin++;
                current->error -= current->dy;
            }
        }
        else {
            current->error -= current->dx;
            while (current->error << 1 > current->dy) {
                current->x_ymin--;
                current->error -= current->dy;
            }
        }
        current = current->next;
    }
}

void ei_draw_polygon(ei_surface_t surface,
                     const ei_linked_point_t* first_point,
                     const ei_color_t color,
                     const ei_rect_t* clipper)
{
    if (first_point != NULL) {

        // If there is at least two points
        if (first_point->next != NULL) {
            // Verify that the first point is different from the last point
            ei_linked_point_t *previous = NULL;
            ei_linked_point_t *last = first_point->next;
            if (last != NULL) {
                while (last->next != NULL) {
                    previous = last;
                    last = last->next;
                }
                if (last->point.x == first_point->point.x && last->point.y == first_point->point.y) {
                    // Unchain last point 
                    free(last);
                    previous->next = NULL;
                }
            }
        }

        ei_bool_t no_transparency = EI_FALSE;
        uint32_t color32 = 0;
        if (color.alpha == NO_TRANSPARENCY) {
            color32 = ei_map_rgba(surface, &color);
            no_transparency = EI_TRUE;
        }

        // Analytic clipper
        ei_linked_point_t *copy_first_point = malloc(sizeof(ei_linked_point_t));
        copy_first_point->point = first_point->point;
        copy_first_point->next = first_point->next;
        ei_linked_point_t *new_first_point = suth_hodg_clip(copy_first_point, clipper);

        if(new_first_point == NULL){
            free(copy_first_point);
            return;
        }

        ei_size_t size = hw_surface_get_size(surface);
        uint32_t *p = (uint32_t*) hw_surface_get_buffer(surface);
        ei_edge_t **tc = create_tc(new_first_point, &size);
        ei_edge_t *tca = NULL;
        // Find the first scanline that intersect the polygon
        uint32_t current_scanline = 0;
        for (uint32_t i = 0; i < size.height; i++) {
            if (tc[i] != NULL) {
                current_scanline = i;
                break;
            }
        }
        p += current_scanline * size.width;
        do {
            // Move TC's edges that begin at current scanline in TCA
            move_edges(&tca, &tc[current_scanline]);
            // Supress TCA's edge such as y_max = y (TC's edges that end at current scanline)
            ei_edge_t *current = tca;
            while (current != NULL) {
                if (current->y_max == current_scanline) {
                    ei_edge_t *edge = pop_edge(&tca, current);
                    current = current->next;
                    free(edge);
                }
                else {
                    current = current->next;
                }
            }
            if (tca != NULL) {
                sort_tca(tca);
                draw_scanline(surface, current_scanline, tca, p, &color, clipper, no_transparency, color32);
                current_scanline++;
                p += size.width;
                update_abscissa(tca);
            }
        } while (tc != NULL && tca != NULL && current_scanline < size.height);
        free_tc(tc, &size);
        free_tca(tca);
        free(copy_first_point);
        if(new_first_point != NULL) free_linked_point(new_first_point);
    }
}

void ei_draw_text(ei_surface_t surface,
                  const ei_point_t* where,
                  const char* text,
                  const ei_font_t font,
                  const ei_color_t* color,
                  const ei_rect_t* clipper)
{
    ei_surface_t text_surface = hw_text_create_surface(text, font ? font : ei_default_font, color);
    ei_size_t text_size = hw_surface_get_size(text_surface);
    ei_rect_t where_rect;
    where_rect = (ei_rect_t){*where, text_size};
    ei_rect_t *text_rect_ptr = NULL;
    if (clipper != NULL) {
        where_rect = ei_rect_intersect(&where_rect, clipper);
        int new_x = minus(where_rect.top_left.x, where->x);
        int new_y = minus(where_rect.top_left.y, where->y);
        ei_rect_t text_rect = {
            {new_x, new_y}, where_rect.size};
        text_rect_ptr = &text_rect;
    }
    ei_copy_surface(surface, &where_rect, text_surface, text_rect_ptr, EI_TRUE);
    hw_surface_free(text_surface);
}

void ei_fill(ei_surface_t surface, const ei_color_t* color, const ei_rect_t* clipper)
{
    ei_size_t size = hw_surface_get_size(surface);
    uint32_t *ptr = (uint32_t*) hw_surface_get_buffer(surface);
    uint32_t col = ei_map_rgba(surface, color);
    uint32_t start_width, start_height, min_width, min_height;
    if (clipper) {
        start_height = clipper->top_left.y;
        start_width = clipper->top_left.x;
        uint32_t min_clipper_height = clipper->size.height + start_height;
        uint32_t min_clipper_width = clipper->size.width + start_width;
        min_height = size.height < min_clipper_height ? size.height : min_clipper_height;
        min_width = size.width < min_clipper_width ? size.width : min_clipper_width;
        ptr += start_height * size.width;
    }
    else {
        start_width = 0;
        start_height = 0;
        min_width = size.width;
        min_height = size.height;
    }
    for (uint32_t i = start_height; i < min_height; i++) {
        for (uint32_t j = start_width; j < min_width; j++) {
            *(ptr + j) = col;
        }
        ptr += size.width;
    }
}

int ei_copy_surface(ei_surface_t destination,
                    const ei_rect_t* dst_rect,
                    const ei_surface_t source,
                    const ei_rect_t* src_rect,
                    const ei_bool_t alpha)
{
    uint32_t *ptr_src = (uint32_t*) hw_surface_get_buffer(source);
    uint32_t *ptr_dst = (uint32_t*) hw_surface_get_buffer(destination);
    ei_size_t src_size = hw_surface_get_size(source);
    ei_size_t dst_size = hw_surface_get_size(destination);
    int height_to_copy, width_to_copy;
    if (src_rect != NULL) {
        height_to_copy = src_rect->size.height;
        width_to_copy = src_rect->size.width;
        ptr_src += src_rect->top_left.x + src_size.width * src_rect->top_left.y;
    }
    else {
        height_to_copy = src_size.height;
        width_to_copy = src_size.width;
    }
    if (dst_rect != NULL) {
        // Test if the source and destination area of copy have the same size
        if (dst_rect->size.height != height_to_copy || dst_rect->size.width != width_to_copy)
            return 1;
        ptr_dst += dst_rect->top_left.x + dst_size.width * dst_rect->top_left.y;
    }
    else {
        if (dst_size.height != height_to_copy || dst_size.width != width_to_copy)
            return 1;
    }
    if (alpha) {
        int irs, igs, ibs, ias, ird, igd, ibd, iad;
        hw_surface_get_channel_indices(source, &irs, &igs, &ibs, &ias);
        hw_surface_get_channel_indices(destination, &ird, &igd, &ibd, &iad);
        for (int i = 0; i < height_to_copy; i++) {
            for (int j = 0; j < width_to_copy; j++) {
                uint32_t *ptr_src_plus_j = ptr_src + j;
                uint32_t *ptr_dst_plus_j = ptr_dst + j;
                unsigned char sa = get_color(ptr_src_plus_j, ias);
                unsigned char sr = get_color(ptr_src_plus_j, irs);
                unsigned char dr = get_color(ptr_dst_plus_j, ird);
                dr = (dr * (255 - sa) + sr * sa) / 255;
                unsigned char sg = get_color(ptr_src_plus_j, igs);
                unsigned char dg = get_color(ptr_dst_plus_j, igd);
                dg = (dg * (255 - sa) + sg * sa) / 255;
                unsigned char sb = get_color(ptr_src_plus_j, ibs);
                unsigned char db = get_color(ptr_dst_plus_j, ibd);
                db = (db * (255 - sa) + sb * sa) / 255;
                const ei_color_t mixed = {dr, dg, db, 0xff};
                uint32_t new_color = ei_map_rgba(destination, &mixed);
                *ptr_dst_plus_j = new_color;
            }
            ptr_dst += dst_size.width;
            ptr_src += src_size.width;
        }
    }
    else {
        for (int i = 0; i < height_to_copy; i++) {
            for (int j = 0; j < width_to_copy; j++) {
                *(ptr_dst + j) = *(ptr_src + j);
            }
            ptr_dst += dst_size.width;
            ptr_src += src_size.width;
        }
    }
    return 0;
}


/* ------------------------------------------------------------------------- */
/* ////////////////////////// CLIPPING POLYGONES \\\\\\\\\\\\\\\\\\\\\\\\\\\ */
/* ------------------------------------------------------------------------- */


/**
 * \brief   Clips all the edges w.r.t one edge of clipping rectangle.
 * 
 * @param       poly_points     The verteces of a polygon.
 * @param       x_min           The abcissa of the clipper first vertical edge.
 * 
 * @return      The verteces of the polygon clipped w.r.t. this edge.
 */
ei_linked_point_t *clipxmin(ei_linked_point_t *poly_points, int x_min)
{
    ei_linked_point_t *new_points = NULL, *current = poly_points, *new_current = NULL;

    while (current != NULL) {
        ei_point_t i = current->point, k;
        if (current->next != NULL) k = current->next->point;
        else k = poly_points->point; // the last point and the first form an edge

        ei_bool_t i_inside = i.x >= x_min;
        ei_bool_t k_inside = k.x >= x_min;
        // Case 1 : both points are inside
        if (i_inside && k_inside) {
            if (new_current == NULL) {
                new_points = malloc(sizeof(ei_linked_point_t));
                new_current = new_points;
            }
            else {
                new_current->next = malloc(sizeof(ei_linked_point_t));
                new_current = new_current->next;
            }
            // k is added
            new_current->next = NULL;
            new_current->point = k;
        }
        // Case 2: only i is outside
        else if (!i_inside && k_inside) {
            if (new_current == NULL) {
                new_points = malloc(sizeof(ei_linked_point_t));
                new_current = new_points;
            }
            else {
                new_current->next = malloc(sizeof(ei_linked_point_t));
                new_current = new_current->next;
            }
            // Point of intersection with edge and k are added
            int y = (k.y - i.y) / (k.x - i.x) * (x_min - i.x) + i.y;
            new_current->point = ei_point(x_min, y);

            new_current->next = malloc(sizeof(ei_linked_point_t));
            new_current = new_current->next;
            new_current->next = NULL;
            new_current->point = k;
        }
        // Case 3: only k is outside
        else if (i_inside && !k_inside) {
            if (new_current == NULL) {
                new_points = malloc(sizeof(ei_linked_point_t));
                new_current = new_points;
            }
            else {
                new_current->next = malloc(sizeof(ei_linked_point_t));
                new_current = new_current->next;
            }
            // Point of intersection with edge is added
            new_current->next = NULL;
            int y = (k.y - i.y) / (k.x - i.x) * (x_min - k.x) + k.y;
            new_current->point = ei_point(x_min, y);
        }
        // Case 4: both points are outside
        else {
            //No points are added
        }
        current = current->next;
    }

    return new_points;
}

/**
 * \brief   Clips all the edges w.r.t one edge of clipping rectangle.
 * 
 * @param       poly_points     The verteces of a polygon.
 * @param       x_max           The abcissa of the clipper second vertical edge.
 * 
 * @return      The verteces of the polygon clipped w.r.t. this edge.
 */
ei_linked_point_t *clipxmax(ei_linked_point_t *poly_points, int x_max)
{
    ei_linked_point_t *new_points = NULL, *current = poly_points, *new_current = NULL;

    while (current != NULL) {
        ei_point_t i = current->point, k;
        if (current->next != NULL) k = current->next->point;
        else k = poly_points->point; // the last point and the first form an edge

        ei_bool_t i_inside = i.x <= x_max;
        ei_bool_t k_inside = k.x <= x_max;
        // Case 1 : both points are inside
        if (i_inside && k_inside) {
            if (new_current == NULL) {
                new_points = malloc(sizeof(ei_linked_point_t));
                new_current = new_points;
            }
            else {
                new_current->next = malloc(sizeof(ei_linked_point_t));
                new_current = new_current->next;
            }
            // k is added
            new_current->next = NULL;
            new_current->point = k;
        }
            // Case 2: only i is outside
        else if (!i_inside && k_inside) {
            if (new_current == NULL) {
                new_points = malloc(sizeof(ei_linked_point_t));
                new_current = new_points;
            }
            else {
                new_current->next = malloc(sizeof(ei_linked_point_t));
                new_current = new_current->next;
            }
            // Point of intersection with edge and k are added
            int y = (k.y - i.y) / (k.x - i.x) * (x_max - k.x) + k.y;
            new_current->point = ei_point(x_max, y);

            new_current->next = malloc(sizeof(ei_linked_point_t));
            new_current = new_current->next;
            new_current->next = NULL;
            new_current->point = k;
        }
        // Case 3: only k is outside
        else if (i_inside && !k_inside) {
            if (new_current == NULL) {
                new_points = malloc(sizeof(ei_linked_point_t));
                new_current = new_points;
            }
            else {
                new_current->next = malloc(sizeof(ei_linked_point_t));
                new_current = new_current->next;
            }
            // Point of intersection with edge is added
            new_current->next = NULL;
            int y = (k.y - i.y) / (k.x - i.x) * (x_max - i.x) + i.y;
            new_current->point = ei_point(x_max, y);
        }
        // Case 4: both points are outside
        else {
            //No points are added
        }
        current = current->next;
    }

    return new_points;
}

/**
 * \brief   Clips all the edges w.r.t one edge of clipping rectangle.
 * 
 * @param       poly_points     The verteces of a polygon.
 * @param       y_min           The ordinate of the clipper first horizontal edge.
 * 
 * @return      The verteces of the polygon clipped w.r.t. this edge.
 */
ei_linked_point_t *clipymin(ei_linked_point_t *poly_points, int y_min)
{
    ei_linked_point_t *new_points = NULL, *current = poly_points, *new_current = NULL;

    while (current != NULL) {
        ei_point_t i = current->point, k;
        if (current->next != NULL) k = current->next->point;
        else k = poly_points->point; // the last point and the first form an edge

        ei_bool_t i_inside = i.y >= y_min;
        ei_bool_t k_inside = k.y >= y_min;
        // Case 1 : both points are inside
        if (i_inside && k_inside) {
            if (new_current == NULL) {
                new_points = malloc(sizeof(ei_linked_point_t));
                new_current = new_points;
            }
            else {
                new_current->next = malloc(sizeof(ei_linked_point_t));
                new_current = new_current->next;
            }
            // k is added
            new_current->next = NULL;
            new_current->point = k;
        }
        // Case 2: only i is outside
        else if (!i_inside && k_inside) {
            if (new_current == NULL) {
                new_points = malloc(sizeof(ei_linked_point_t));
                new_current = new_points;
            }
            else {
                new_current->next = malloc(sizeof(ei_linked_point_t));
                new_current = new_current->next;
            }
            // Point of intersection with edge and k are added
            int x = (k.x - i.x) / (k.y - i.y) * (y_min - i.y) + i.x;
            new_current->point = ei_point(x, y_min);

            new_current->next = malloc(sizeof(ei_linked_point_t));
            new_current = new_current->next;
            new_current->next = NULL;
            new_current->point = k;
        }
        // Case 3: only k is outside
        else if (i_inside && !k_inside) {
            if (new_current == NULL) {
                new_points = malloc(sizeof(ei_linked_point_t));
                new_current = new_points;
            }
            else {
                new_current->next = malloc(sizeof(ei_linked_point_t));
                new_current = new_current->next;
            }
            // Point of intersection with edge is added
            new_current->next = NULL;
            int x = (k.x - i.x) / (k.y - i.y) * (y_min - k.y) + k.x;
            new_current->point = ei_point(x, y_min);
        }
        // Case 4: both points are outside
        else {
            //No points are added
        }
        current = current->next;
    }

    return new_points;
}

/**
 * \brief   Clips all the edges w.r.t one edge of clipping rectangle.
 * 
 * @param       poly_points     The verteces of a polygon.
 * @param       y_max           The ordinate of the clipper second horizontal edge.
 * 
 * @return      The verteces of the polygon clipped w.r.t. this edge.
 */
ei_linked_point_t *clipymax(ei_linked_point_t *poly_points, int y_max)
{
    ei_linked_point_t *new_points = NULL, *current = poly_points, *new_current = NULL;

    while (current != NULL) {
        ei_point_t i = current->point, k;
        if (current->next != NULL) k = current->next->point;
        else k = poly_points->point; // the last point and the first form an edge

        ei_bool_t i_inside = i.y <= y_max;
        ei_bool_t k_inside = k.y <= y_max;
        // Case 1 : both points are inside
        if (i_inside && k_inside) {
            if (new_current == NULL) {
                new_points = malloc(sizeof(ei_linked_point_t));
                new_current = new_points;
            }
            else {
                new_current->next = malloc(sizeof(ei_linked_point_t));
                new_current = new_current->next;
            }
            // k is added
            new_current->next = NULL;
            new_current->point = k;
        }
        // Case 2: only i is outside
        else if (!i_inside && k_inside) {
            if (new_current == NULL) {
                new_points = malloc(sizeof(ei_linked_point_t));
                new_current = new_points;
            }
            else {
                new_current->next = malloc(sizeof(ei_linked_point_t));
                new_current = new_current->next;
            }
            // Point of intersection with edge and k are added
            int x = (k.x - i.x) / (k.y - i.y) * (y_max - k.y) + k.x;
            new_current->point = ei_point(x, y_max);

            new_current->next = malloc(sizeof(ei_linked_point_t));
            new_current = new_current->next;
            new_current->next = NULL;
            new_current->point = k;
        }
        // Case 3: only second point is outside
        else if (i_inside && !k_inside) {
            if (new_current == NULL) {
                new_points = malloc(sizeof(ei_linked_point_t));
                new_current = new_points;
            }
            else {
                new_current->next = malloc(sizeof(ei_linked_point_t));
                new_current = new_current->next;
            }
            // Point of intersection with edge is added
            new_current->next = NULL;
            int x = (k.x - i.x) / (k.y - i.y) * (y_max - i.y) + i.x;
            new_current->point = ei_point(x, y_max);
        }
        // Case 4: both points are outside
        else {
            //No points are added
        }
        current = current->next;
    }

    return new_points;
}

ei_linked_point_t *suth_hodg_clip(ei_linked_point_t *poly_points, const ei_rect_t *clipper)
{
    if (clipper == NULL) return poly_points;


    ei_linked_point_t *linked1, *linked2, *linked3, *linked4;

    linked1 = clipxmin(poly_points, clipper->top_left.x);
    linked2 = clipxmax(linked1, clipper->top_left.x + clipper->size.width);
    free_linked_point(linked1);
    linked3 = clipymin(linked2, clipper->top_left.y);
    free_linked_point(linked2);
    linked4 = clipymax(linked3, clipper->top_left.y + clipper->size.height);
    free_linked_point(linked3);

    return linked4;
}
