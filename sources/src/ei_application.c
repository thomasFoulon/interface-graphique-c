#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ei_application.h"
#include "ei_event.h"
#include "ei_offscreen.h"
#include "ei_tools.h"
#include "ei_utils.h"
#include "ei_widgetfunctions.h"
#include "hw_interface.h"


ei_widget_t         *root_widget;
ei_surface_t        main_window;
ei_size_t           window_size;

ei_bool_t           stop_app        = EI_FALSE;
ei_eventtype_t      last_event_type = ei_ev_none;

ei_linked_rect_t    *head_to_update = NULL, *current_to_update = NULL;



/**
 * \brief   Free memory of linked rect.
 * 
 * @param       to_free     The rect to free.
 */
void free_linked_rect(ei_linked_rect_t *to_free)
{
    if (to_free == NULL) return;

    if (to_free->next != NULL) free_linked_rect(to_free->next);
    free(to_free);
}

void ei_app_invalidate_rect(ei_rect_t* rect)
{
    if (rect == NULL) return;
    if (rect->size.width <= 0 || rect->size.height <= 0) return;

    if (head_to_update == NULL) {
        current_to_update = malloc(sizeof(ei_linked_rect_t));
        head_to_update = current_to_update;
    }
    else {
        current_to_update->next = malloc(sizeof(ei_linked_rect_t));
        current_to_update = current_to_update->next;
    }
    current_to_update->rect = *rect;
    current_to_update->next = NULL;
}

void ei_app_create(ei_size_t* main_window_size, ei_bool_t fullscreen)
{
    if (main_window_size == NULL) return; // TODO ???

    // Init access to hardware
    hw_init();

    window_size = *main_window_size;

    // Create the main window
    main_window = hw_create_window(main_window_size, fullscreen);

    // Offscreen for events purposes
    ei_offscreen_init(main_window, main_window_size);

    // Registers all classes
    ei_frame_register_class();
    ei_button_register_class();
    ei_toplevel_register_class();

    // Create the root widget
    root_widget = ei_widget_create(CLASS_NAME_FRAME, NULL);
    root_widget->requested_size = *main_window_size;
    ei_place(root_widget, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
}

/**
 * \brief   Call recursively the placer run function.
 * 
 * @param   widget      The current widget to call the placer run on.
 */
void placer_run_widget_rec(ei_widget_t *widget)
{
    ei_placer_run(widget);
    ei_widget_t *current_child = widget->children_head;
    while (current_child != NULL) {
        placer_run_widget_rec(current_child);
        current_child = current_child->next_sibling;
    }
}

/**
 * \brief   Call recursively the draw function.
 * 
 * @param       widget      Widget to draw.
 * @param       clipper     Clipper used by draw function.
 */
void draw_widget_rec(ei_widget_t *widget, ei_rect_t *clipper)
{
    widget->wclass->drawfunc(widget, main_window, ei_offscreen_get(), clipper);
    ei_widget_t *current_child = widget->children_head;
    while (current_child != NULL) {
        draw_widget_rec(current_child, clipper);
        current_child = current_child->next_sibling;
    }
}

/**
 * \brief   Call the handle function of picked widget.
 * 
 * @param       event       Trigger event. 
 */
void handle_event(ei_event_t *event)
{
    ei_bool_t placed_event;
    ei_widget_t *picked = NULL;
    if (event->type == ei_ev_last) event->type = last_event_type;
    switch (event->type) {
    case ei_ev_mouse_buttondown: case ei_ev_mouse_buttonup: case ei_ev_mouse_move:
        placed_event = EI_TRUE;
        break;
    default:
        placed_event = EI_FALSE;
    }
    last_event_type = event->type;
    if (placed_event) {
        picked = ei_widget_pick(&event->param.mouse.where);
        if (picked != NULL) {
            if (picked->wclass->handlefunc != NULL) picked->wclass->handlefunc(picked, event);
        }
    }
    else if (ei_event_get_default_handle_func() != NULL) ei_event_get_default_handle_func()(event);
}

/**
 * \brief   Take rectangles present in the list of rectagle to update and create the adapted clipper.
 * 
 * @return      The clipper made by rectangles of invalidate rect.
 */
ei_rect_t *get_clipper()
{
    if (head_to_update == NULL) return NULL;
    int min_x = window_size.width, min_y = window_size.height, max_x = 0, max_y = 0, x_left, x_right, y_top, y_bottom;
    ei_linked_rect_t *current = head_to_update, *next = NULL;
    while (current != NULL) {
        x_left = current->rect.top_left.x;
        x_right = x_left + current->rect.size.width;
        y_top = current->rect.top_left.y;
        y_bottom = y_top + current->rect.size.height;
        if (x_left < min_x) min_x = x_left;
        if (x_right > max_x) max_x = x_right;
        if (y_top < min_y) min_y = y_top;
        if (y_bottom > max_y) max_y = y_bottom;
        next = current->next;
        free(current);
        current = next;
    }
    if (max_x > window_size.width) {
        max_x = window_size.width;
    }
    if (max_y > window_size.height) {
        max_y = window_size.height;
    }
    if (min_x < 0)
        min_x = 0;
    if (min_y < 0)
        min_y = 0;
    ei_rect_t *clipper = malloc(sizeof(ei_rect_t));
    *clipper = ei_rect(ei_point(min_x, min_y), ei_size(max_x - min_x, max_y - min_y));

    head_to_update = NULL;

    return clipper;
}

void ei_app_run()
{
    ei_event_t event;
    // At first we draw all the screen
    placer_run_widget_rec(root_widget);
//    ei_app_invalidate_rect(&root_widget->screen_location);
    // Main loop
    while (!stop_app) {
        placer_run_widget_rec(root_widget);
        hw_surface_lock(main_window);
        hw_surface_lock(ei_offscreen_get());
        ei_rect_t *clipper = get_clipper();
        if (clipper != NULL) {
            draw_widget_rec(root_widget, clipper);
            free(clipper);
        }
        hw_surface_unlock(main_window);
        hw_surface_unlock(ei_offscreen_get());
        hw_surface_update_rects(main_window, NULL);
        hw_surface_update_rects(ei_offscreen_get(), NULL);
        hw_event_wait_next(&event);
        handle_event(&event);
        // DEBUG
        //        stop_app = EI_TRUE;
    }
}

void ei_app_quit_request()
{
    stop_app = EI_TRUE;
}

ei_widget_t* ei_app_root_widget()
{
    return root_widget;
}

ei_surface_t ei_app_root_surface()
{
    return main_window;
}

void ei_app_free()
{
    // Free the widgets
    ei_widget_destroy(root_widget);
    // The first class should be the frame (first registered in ei_app_create)
    ei_widgetclass_t *widget_class = ei_widgetclass_from_name(CLASS_NAME_FRAME), *next;
    while (widget_class != NULL) {
        next = widget_class->next;
        free(widget_class);
        widget_class = next;
    }
    hw_quit();
}
