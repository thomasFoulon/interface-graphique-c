#include <stdlib.h>
#include <stdio.h>

#include "ei_utils.h"
#include "ei_print.h"
#include "ei_types.h"
#include "ei_edge.h"



void print_ei_linked_point(ei_linked_point_t *first_point)
{
    ei_linked_point_t *current = first_point;
    while (current != NULL) {
        printf("(%d, %d)", current->point.x, current->point.y);
        current = current->next;
    }
    printf("\n");
}

void print_ei_double_circular_linked_list(ei_double_linked_point_t *first_point)
{
    ei_double_linked_point_t *current = first_point;
    do {
        printf("(%d, %d)", current->point.x, current->point.y);
        current = current->next;
    } while (current != first_point);
    printf("\n");
}

void print_edge(ei_edge_t *first_edge, uint32_t indice)
{
    ei_edge_t *current = first_edge;
    printf("%u", indice);
    while (current != NULL) {
        //        printf(" --> %p, ", current);
        printf(" --> %d, ", current->y_max);
        printf("%d, ", current->x_ymin);
        printf("%d, ", current->x_ymax);
        printf("%d, ", current->dx);
        printf("%d, ", current->dy);
        printf("%d, ", current->error);
        //        printf("%.1f, ", current->reciprocal_slope);
        //        printf("%p", current->next);
        current = current->next;
    }
    printf("\n");
}

void print_tc(ei_edge_t **tc, ei_size_t *size)
{
    for (uint32_t i = 0; i < size->height; i++) {
        if (tc[i] == NULL) {
            printf("%u -- NULL\n", i);
        }
        else {
            print_edge(tc[i], i);
        }
    }
}
