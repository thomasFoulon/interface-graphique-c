/**
 *  @file	ei_widgetclasses.h
 *  @brief	De.
 *
 *  \author
 *  Created by Julien Bitaillou, Thomas Foulon and Mathilde Grapin on 24.05.19.
 *  Copyright 2019 Ensimag. All rights reserved.
 *
 */

#ifndef EI_WIDGETCLASSES_H
#define	EI_WIDGETCLASSES_H


#include "ei_widget.h"


#define CLASS_NAME_FRAME "frame"
#define CLASS_NAME_BUTTON "button"
#define CLASS_NAME_RESIZE "resize"
#define CLASS_NAME_TOP_LEVEL "toplevel"


/**
 * \brief	Specialized class of widget.
 *              Class frame behave as a container.
 */
typedef struct ei_frame_widget_t {
    
    ei_widget_t         widget;
    
    ei_color_t          color;
    int                 border_width;
    ei_relief_t         relief;
    char*		text;
    ei_font_t           text_font;
    ei_color_t          text_color;
    ei_anchor_t         text_anchor;
    ei_surface_t	img;
    ei_rect_t*		img_rect;
    ei_anchor_t         img_anchor;
    
} ei_frame_widget_t;

/**
 * \brief	Specialized class of widget.
 *              Class button.
 */
typedef struct ei_button_widget_t {
    
    ei_widget_t         widget;
    
    ei_color_t          color;
    int                 border_width;
    int                 corner_radius;
    ei_relief_t         relief;
    char*               text;
    ei_font_t		text_font;
    ei_color_t		text_color;
    ei_anchor_t         text_anchor;
    ei_surface_t	img;
    ei_rect_t*		img_rect;
    ei_anchor_t         img_anchor;
    ei_callback_t       callback;
    void*		user_param;
    
} ei_button_widget_t;

/**
 * \brief	Specialized class of widget.
 *              Class top level is a container that we can move, 
 *              potentially resize and close.
 */
typedef struct ei_top_level_widget_t {
    
    ei_widget_t         widget;
    
    ei_color_t          color;
    int                 border_width;
    char*               title;
    ei_bool_t           closable;
    ei_axis_set_t       resizable;
    ei_size_t*          min_size;
    
    ei_widget_t*        button_close;
    ei_widget_t*        resize;

} ei_top_level_widget_t;

/**
 * \brief	Specialized class of widget.
 *              Class resize is a special type of button where is intern event 
 *              function is to resize its top level parent.
 */
typedef struct ei_resize_widget_t {
    
    ei_widget_t         widget;
    
    ei_color_t          color;
    int                 border_width;
    int                 corner_radius;
    ei_relief_t         relief;
    char*               text;
    ei_font_t		text_font;
    ei_color_t		text_color;
    ei_anchor_t         text_anchor;
    ei_surface_t	img;
    ei_rect_t*		img_rect;
    ei_anchor_t         img_anchor;
    ei_callback_t       callback;
    void*		user_param;
    
} ei_resize_widget_t;




#endif	/* EI_WIDGETCLASSES_H */

