/**
 *  @file	ei_widgetdraw.h
 *  @brief	Permits to draw widgets like buttons.
 *
 *  \author
 *  Created by Julien Bitaillou, Thomas Foulon and Mathilde Grapin on 23.05.19.
 *  Copyright 2019 Ensimag. All rights reserved.
 *
 */

#ifndef EI_WIDGETFUNCTIONS_H
#define	EI_WIDGETFUNCTIONS_H


#include "ei_event.h"
#include "ei_widgetclasses.h"



/* ------------------------------------------------------------------------- */
/* ///////////////////////////////// FRAME \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */
/* ------------------------------------------------------------------------- */

/**
 * \brief	Allocates a block of memory that is big enough to store the
 *		attributes of a frame. After allocation, the function initialize the memory to 0.
 *
 * @return	A block of memory with all bytes set to 0.
 */
void*       ei_alloc_frame      ();

/**
 * \brief	A function that releases the memory used by a frame before it is destroyed.
 *		The \ref ei_widget_t structure itself, passed as parameter, must *not* by freed by
 *		these functions.
 *
 * @param	widget	The frame which resources are to be freed.
 */
void        ei_release_frame    (struct ei_widget_t* widget);

/**
 * \brief	Draws a frame.
 *
 * @param	widget		A pointer to the widget instance to draw.
 * @param	surface		Where to draw the widget. The actual location of the widget in the
 *				surface is stored in its "screen_location" field.
 * @param	clipper		If not NULL, the drawing is restricted within this rectangle
 *				(expressed in the surface reference frame).
 */
void        ei_draw_frame      (struct ei_widget_t*	widget,
                                ei_surface_t            surface,
                                ei_surface_t            pick_surface,
                                ei_rect_t*		clipper);

/**
 * \brief	A function that sets the default values for a frame.
 *
 * @param	widget		A pointer to the widget instance to initialize.
 */
void        ei_setdefaults_frame	(struct ei_widget_t* widget);

/**
 * @brief	A function that is called in response to an event. This function
 *		is internal to the library. It implements the generic behavior of
 *		a frame.
 *
 * @param	widget		The widget for which the event was generated.
 * @param	event		The event containing all its parameters (type, etc.)
 *
 * @return			A boolean telling if the event was consumed by the callback or not.
 *				If TRUE, the library does not try to call other callbacks for this
 *				event. If FALSE, the library will call the next callback registered
 *				for this event, if any.
 *				Note: The callback may execute many operations and still return
 *				FALSE, or return TRUE without having done anything.
 */
ei_bool_t   ei_sethandlefunc_frame (struct ei_widget_t* widget,
                                     struct ei_event_t* event);

/**
 * \brief 	A function that is called to notify the widget that its geometry has been modified
 *		by its geometry manager. Can set to NULL in \ref ei_widgetclass_t.
 *
 * @param	widget		The widget instance to notify of a geometry change.
 * @param	rect		The new rectangular screen location of the widget
 *				(i.e. = widget->screen_location).
 */
void        ei_setgeomnotifyfunc_frame	(struct ei_widget_t* widget,
                                         ei_rect_t           rect);


/* ------------------------------------------------------------------------- */
/* //////////////////////////////// BUTTON \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */
/* ------------------------------------------------------------------------- */

/**
 * \brief	Allocates a block of memory that is big enough to store the
 *		attributes of a button. After allocation, the function initialize the memory to 0.
 *
 * @return	A block of memory with all bytes set to 0.
 */
void*       ei_alloc_button     ();

/**
 * \brief	A function that releases the memory used by a button before it is destroyed.
 *		The \ref ei_widget_t structure itself, passed as parameter, must *not* by freed by
 *		these functions.
 *
 * @param	widget	The frame which resources are to be freed.
 */
void        ei_release_button    (struct ei_widget_t* widget);

/**
 * \brief	Draws a button.
 *
 * @param	widget		A pointer to the widget instance to draw.
 * @param	surface		Where to draw the widget. The actual location of the widget in the
 *				surface is stored in its "screen_location" field.
 * @param	clipper		If not NULL, the drawing is restricted within this rectangle
 *				(expressed in the surface reference frame).
 */
void        ei_draw_button      (struct ei_widget_t*	widget,
                                ei_surface_t            surface,
                                ei_surface_t            pick_surface,
                                ei_rect_t*		clipper);

/**
 * \brief	A function that sets the default values for a button.
 *
 * @param	widget		A pointer to the widget instance to initialize.
 */
void        ei_setdefaults_button (struct ei_widget_t* widget);

/**
 * @brief	A function that is called in response to an event. This function
 *		is internal to the library. It implements the generic behavior of
 *		a button.
 *
 * @param	widget		The widget for which the event was generated.
 * @param	event		The event containing all its parameters (type, etc.)
 *
 * @return			A boolean telling if the event was consumed by the callback or not.
 *				If TRUE, the library does not try to call other callbacks for this
 *				event. If FALSE, the library will call the next callback registered
 *				for this event, if any.
 *				Note: The callback may execute many operations and still return
 *				FALSE, or return TRUE without having done anything.
 */
ei_bool_t   ei_sethandlefunc_button (struct ei_widget_t* widget,
                                     struct ei_event_t* event);

/**
 * \brief 	A function that is called to notify the widget that its geometry has been modified
 *		by its geometry manager. Can set to NULL in \ref ei_widgetclass_t.
 *
 * @param	widget		The widget instance to notify of a geometry change.
 * @param	rect		The new rectangular screen location of the widget
 *				(i.e. = widget->screen_location).
 */
void        ei_setgeomnotifyfunc_button	(struct ei_widget_t* widget,
                                         ei_rect_t           rect);


/* ------------------------------------------------------------------------- */
/* ////////////////////////////// TOP LEVEL \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */
/* ------------------------------------------------------------------------- */

static const int           k_default_toplevel_titlebar_height       = 30;	///< The default height of title bar.
static const ei_color_t    k_default_toplevel_titlebar_color        = { 0x63, 0x69, 0x70, 0xff };   ///< The default color of title bar.
static const ei_color_t    k_default_toplevel_closebutton_color     = { 0xDD, 0x00, 0x00, 0xff };   ///< The default color of closing button.

/*
**
 * \brief	Allocates a block of memory that is big enough to store the
 *		attributes of a top level. After allocation, the function initialize the memory to 0.
 *
 * @return	A block of memory with all bytes set to 0.
 */
void*       ei_alloc_top_level      ();

/**
 * \brief	A function that releases the memory used by a frame before it is destroyed.
 *		The \ref ei_widget_t structure itself, passed as parameter, must *not* by freed by
 *		these functions.
 *
 * @param	widget	The top level which resources are to be freed.
 */
void        ei_release_top_level    (struct ei_widget_t* widget);

/**
 * \brief	Draws a top_level.
 *
 * @param	widget		A pointer to the widget instance to draw.
 * @param	surface		Where to draw the widget. The actual location of the widget in the
 *				surface is stored in its "screen_location" field.
 * @param	clipper		If not NULL, the drawing is restricted within this rectangle
 *				(expressed in the surface reference frame).
 */
void        ei_draw_top_level (struct ei_widget_t*	widget,
                                ei_surface_t            surface,
                                ei_surface_t            pick_surface,
                                ei_rect_t*		clipper);

/**
 * \brief	A function that sets the default values for a top level.
 *
 * @param	widget		A pointer to the widget instance to initialize.
 */
void        ei_setdefaults_top_level	(struct ei_widget_t* widget);

/**
 * @brief	A function that is called in response to an event. This function
 *		is internal to the library. It implements the generic behavior of
 *		a top_level.
 *
 * @param	widget		The widget for which the event was generated.
 * @param	event		The event containing all its parameters (type, etc.)
 *
 * @return			A boolean telling if the event was consumed by the callback or not.
 *				If TRUE, the library does not try to call other callbacks for this
 *				event. If FALSE, the library will call the next callback registered
 *				for this event, if any.
 *				Note: The callback may execute many operations and still return
 *				FALSE, or return TRUE without having done anything.
 */
ei_bool_t   ei_sethandlefunc_top_level (struct ei_widget_t* widget,
                                        struct ei_event_t* event);

/**
 * \brief 	A function that is called to notify the widget that its geometry has been modified
 *		by its geometry manager. Can set to NULL in \ref ei_widgetclass_t.
 *
 * @param	widget		The widget instance to notify of a geometry change.
 * @param	rect		The new rectangular screen location of the widget
 *				(i.e. = widget->screen_location).
 */
void        ei_setgeomnotifyfunc_top_level (struct ei_widget_t* widget,
                                           ei_rect_t           rect);


/* ------------------------------------------------------------------------- */
/* //////////////////////////////// RESIZE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */
/* ------------------------------------------------------------------------- */

/**
 * \brief	Allocates a block of memory that is big enough to store the
 *		attributes of a resize widget. After allocation, the function 
 *              initialize the memory to 0.
 *
 * @return	A block of memory with all bytes set to 0.
 */
void*      ei_alloc_resize     ();

/**
 * \brief	A function that releases the memory used by a resize widget before it is destroyed.
 *		The \ref ei_widget_t structure itself, passed as parameter, must *not* by freed by
 *		these functions.
 *
 * @param	widget	The frame which resources are to be freed.
 */
void        ei_release_resize    (struct ei_widget_t* widget);

/**
 * \brief	Draws a resize widget.
 *
 * @param	widget		A pointer to the widget instance to draw.
 * @param	surface		Where to draw the widget. The actual location of the widget in the
 *				surface is stored in its "screen_location" field.
 * @param	clipper		If not NULL, the drawing is restricted within this rectangle
 *				(expressed in the surface reference frame).
 */
void        ei_draw_resize     (struct ei_widget_t*	widget,
                                ei_surface_t            surface,
                                ei_surface_t            pick_surface,
                                ei_rect_t*		clipper);

/**
 * \brief	A function that sets the default values for a resize widget.
 *
 * @param	widget		A pointer to the widget instance to initialize.
 */
void        ei_setdefaults_resize (struct ei_widget_t* widget);

/**
 * @brief	A function that is called in response to an event. This function
 *		is internal to the library. It implements the generic behavior of
 *		a resize widget.
 *
 * @param	widget		The widget for which the event was generated.
 * @param	event		The event containing all its parameters (type, etc.)
 *
 * @return			A boolean telling if the event was consumed by the callback or not.
 *				If TRUE, the library does not try to call other callbacks for this
 *				event. If FALSE, the library will call the next callback registered
 *				for this event, if any.
 *				Note: The callback may execute many operations and still return
 *				FALSE, or return TRUE without having done anything.
 */
ei_bool_t   ei_sethandlefunc_resize (struct ei_widget_t* widget,
                                     struct ei_event_t* event);

/**
 * \brief 	A function that is called to notify the widget that its geometry has been modified
 *		by its geometry manager. Can set to NULL in \ref ei_widgetclass_t.
 *
 * @param	widget		The widget instance to notify of a geometry change.
 * @param	rect		The new rectangular screen location of the widget
 *				(i.e. = widget->screen_location).
 */
void        ei_setgeomnotifyfunc_resize	(struct ei_widget_t* widget,
                                         ei_rect_t           rect);




#endif	/* EI_WIDGETFUNCTIONS_H */
