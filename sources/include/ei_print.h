/**
 *  @file	ei_tools.h
 *  @brief	Bundle of print and debug function.
 *
 *  \author
 *  Created by Julien Bitaillou, Thomas Foulon and Mathilde Grapin on 22.05.19.
 *  Copyright 2019 Ensimag. All rights reserved.
 *
 */

#ifndef EI_PRINT_H
#define	EI_PRINT_H


#include "ei_types.h"
#include "ei_edge.h"



/**
 * \brief   Print the linked list of points.
 *
 * @param	list            The first point of the list.
 */
void        print_ei_linked_point(ei_linked_point_t *first_point);

/**
 * \brief   Print the double ciruclar linked list of points.
 *
 * @param	list            The first point of the list.
 */
void        print_ei_double_circular_linked_list(ei_double_linked_point_t *first_point);

/**
 * \brief   Print the linked list of edges.
 *
 * @param	first_edge      The first edge of the list.
 * 
 * @param       indice          Indice of the scanline, just use to debug.
 */
void        print_edge(ei_edge_t *first_edge, uint32_t indice);

/**
 * \brief   Print the table of edges.
 *
 * @param	tc              The table of edges.
 * 
 * @param	size            The size of the table of edge.
 */
void        print_tc(ei_edge_t **tc, ei_size_t *size);




#endif	/* EI_PRINT_H */


