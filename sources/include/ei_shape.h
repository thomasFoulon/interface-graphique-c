/**
 *  @file	ei_shape.h
 *  @brief	Determine the set of points of a specific shape.
 *
 *  \author
 *  Created by Julien Bitaillou, Thomas Foulon and Mathilde Grapin on 20.05.19.
 *  Copyright 2019 Ensimag. All rights reserved.
 *
 */

#ifndef EI_SHAPE_H
#define EI_SHAPE_H


#include <stdint.h>
#include "ei_types.h"



/**
 * @brief	Represents the part to draw of the shape especially the rounded frame.
 */
typedef enum{TOP, BOTTOM, ALL} ei_shape_part_t;

/**
 * \brief	Return a set of points which represents an arc.
 *
 * @param center          The center of the arc.
 * @param radius          The radius of the arc.
 * @param octants         The octants of the arc to draw.
 *
 * @return                The head of a linked list of points which represents the arc.
 */
ei_linked_point_t *ei_get_arc(ei_point_t center, int radius, ei_bool_t octants[8]);

/**
 * \brief	Return a set of points which represents an arc.
 *
 * @param rect            The rectangle to build the rounded frame.
 * @param radius          The radius of the corners.
 * @param part            The part of the rounded frame to draw.
 *
 * @return                The head of a linked list of points which represents a rounded frame.
 */
ei_linked_point_t *ei_get_rounded_frame(ei_rect_t rect, int radius, ei_shape_part_t part);

/**
 * \brief	Return a set of points which represents a rectangle.
 *
 * @param rect            The rectangle to build the points of the rectangle.
 *
 * @return                The head of a linked list of points which represents a rectangle.
 *                        The list begin with the top left point. 
 */
ei_linked_point_t *ei_get_rectangle_frame(ei_rect_t *rect);

/**
 * \brief	Return a set of points which represents a bar title.
 *
 * @param rect            The rectangle to build the points of the bar title.
 *
 * @return                The head of a linked list of points which represents a bar title.
 */
ei_linked_point_t *ei_get_bar_title(ei_rect_t *rect);




#endif /* EI_SHAPE_H */
