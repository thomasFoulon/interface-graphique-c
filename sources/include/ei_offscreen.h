/**
 *  @file	ei_offscreen.h
 *  @brief	Offscreen management.
 *
 *  \author
 *  Created by Julien Bitaillou, Thomas Foulon and Mathilde Grapin on 03.06.19.
 *  Copyright 2019 Ensimag. All rights reserved.
 *
 */

#ifndef EI_OFFSCREEN_H
#define	EI_OFFSCREEN_H


#include "hw_interface.h"



/**
 * \brief                 Initialize the offscreen of the application.
 * 
 * @param   main_window   The main window of the application.
 * @param   size          The size of the offscreen.
 */
void        ei_offscreen_init(ei_surface_t main_window, ei_size_t *size);

/**
 * \brief	Return the offscreen of the application.
 *
 * @return	The offscreen.
 */
ei_surface_t ei_offscreen_get();




#endif	/* EI_OFFSCREEN_H */

