/**
 *  @file	ei_tools.h
 *  @brief	Free and others functions for types.
 *
 *  \author
 *  Created by Julien Bitaillou, Thomas Foulon and Mathilde Grapin on 24.05.19.
 *  Copyright 2019 Ensimag. All rights reserved.
 *
 */

#ifndef EI_TOOLS_H
#define	EI_TOOLS_H


#include "ei_types.h"
#include "ei_widget.h"



/**
 * \brief   Compares two \ref ei_point_t (first by abscissa).
 *
 * @param   point1   The first point.
 * @param   point2   The second point.
 * 
 * @return  -1 if point1 < point2, 1 if point1 > point2, 0 if they are equals.
 */
int         compare_points(ei_point_t point1, ei_point_t point2);

/**
 * \brief   Compares two \ref ei_size_t (first by width attribute).
 *
 * @param   size1   The first size.
 * @param   size2   The second size.
 * 
 * @return  -1 if size1 < size2, 1 if size1 > size2, 0 if they are equals.
 */
int         compare_sizes(ei_size_t size1, ei_size_t size2);

/**
 * \brief   Compares two \ref ei_rect_t (first by top_left attribute).
 *
 * @param   rect1   The first rectangle.
 * @param   rect2   The second rectangle.
 * 
 * @return  -1 if rect1 < rect2, 1 if rect1 > rect2, 0 if they are equals.
 */
int         compare_rects(ei_rect_t rect1, ei_rect_t rect2);

/**
 * \brief   Free memory of linked list of points.
 *
 * @param	first_point 	A pointer toward the fisrt point of the list.
 */
void        free_linked_point(ei_linked_point_t *first_element);

/**
 * \brief   Compare two integer and return the minimum.
 *
 * @param	a               Integer to compare.
 * @param       b               Integer to compare.
 * 
 * @return      Min between a and b.
 */
int         min(int a, int b);

/**
 * \brief   Compare two integer and return the maximum.
 *
 * @param	a               Integer to compare.
 * @param       b               Integer to compare.
 * 
 * @return      Max between a and b.
 */
int         max(int a, int b);

/**
 * \brief   Difference between a and b if a is greater than b.
 *
 * @param	a               Integer to compare.
 * @param       b               Integer to compare.
 * 
 * @return      Difference between a and b if a is greater than b.
 */
int         minus(int a, int b);

/**
 * \brief   Intersection of two rectangle.
 * 
 * @param       rect1           Rectangle to compare
 * @param       rect2           Rectangle to compare
 * 
 * @return      A rect that is the intersection of rect1 and rect2
 */
ei_rect_t ei_rect_intersect(ei_rect_t* rect1, const ei_rect_t* rect2);

/**
 * \brief   Unchain the widget from its parents childen linked list and return it.
 * 
 * @param       widget          Widget to remove
 * 
 * @return      The unchained widget.
 */
ei_widget_t *remove_from_parent(ei_widget_t* widget);




#endif	/* EI_TOOLS_H */

