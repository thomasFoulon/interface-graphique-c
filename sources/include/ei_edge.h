/**
 *  @file	ei_edge.h
 *  @brief	Dertermine the edge of a polygone.
 *
 *  \author
 *  Created by Julien Bitaillou, Thomas Foulon and Mathilde Grapin on 21.05.19.
 *  Copyright 2019 Ensimag. All rights reserved.
 *
 */

#ifndef EI_EDGE_H
#define EI_EDGE_H


#include <stdint.h>
#include "ei_types.h"

#include "hw_interface.h"



/**
 * @brief	A edge of a polygone plus a pointer to create a linked list.
 */
typedef struct ei_edge_t {
    uint32_t y_max; ///< Maximal ordinate between both vertices.
    uint32_t x_ymin; ///< Abscissa of vertex at minimal ordinate.
    uint32_t x_ymax; ///< Abscissa of vertex at maximal ordinate.
    int32_t dx; ///< difference between x_end and x_start of the segment.
    int32_t dy; ///< difference between y_end and y_start of the segment.
    int32_t error; ///< error used to update x_ymin.
    //    double			reciprocal_slope; ///< Reciprocal of edge's slope: dx / dy.
    struct ei_edge_t* next; ///< Pointer towards the next edge in the linked list.
} ei_edge_t;

/**
 * @brief	A point plus two pointer to create a double linked list.
 */
typedef struct ei_double_linked_point_t {
    ei_point_t point; ///< The point.
    struct ei_double_linked_point_t* next; ///< The pointer to the next element in the linked list.
    struct ei_double_linked_point_t* previous; ///< The pointer to the previous element in the linked list.
} ei_double_linked_point_t;

/**
 * \brief   Push an edge at the beginning of the linked list.
 *
 * @param	tc 	The table of edge to attach the linked list of edge.
 * 
 * @param       start   The beginning of the edge.
 * 
 * @param       end     The end of the edge.
 */
void        push_front_edge_tc(ei_edge_t                      **tc,
                               const ei_double_linked_point_t *start,
                               const ei_double_linked_point_t *end);

/**
 * \brief   Push the edge at the begining of the linked list of edge.
 *
 * @param	list                    Pointer toward the pointer of the first element of 
 *                                      the linked list of edge.
 * 
 * @param       edge                    The edge to push.
 */
void        push_front_edge(ei_edge_t **list, ei_edge_t *edge);

/**
 * \brief   Pop the given edge out of the linked list.
 *
 * @param	tca     The linked list.
 * 
 * @param       current The edge to pop.
 * 
 * @return              Return the popped edge.
 */
ei_edge_t*  pop_edge(ei_edge_t **tca, ei_edge_t *edge);

/**
 * \brief   Move all edges from the source list into the destination list. As
 *          result the source list will be empty.
 *
 * @param	list_destination 	A pointer toward the first element of 
 *                                      the destination linked list of edge.
 * 
 * @param       list_source             A pointer toward the last element of
 *                                      the source linked list of edge.
 */
void        move_edges(ei_edge_t **list_destination, ei_edge_t **list_source);

/**
 * \brief   Init the edge table used to trace polygone with all edges of polygone.
 *
 * @param	surface 	Where to draw the line. The surface must be *locked* by
 *				\ref hw_surface_lock.
 * @param	first_point 	The head of a linked list of the points of the line. It can be NULL
 *				(i.e. draws nothing), can have a single point, or more.
 *				If the last point is the same as the first point, then this pixel is
 *				drawn only once. 
 * 
 * @return                      Retrun the create table of edges.
 */
ei_edge_t** create_tc(const ei_linked_point_t *first_point, ei_size_t *size);

/**
 * \brief   Create a double circular linked list of points from a simple linked list of points.
 *
 * @param	first_point 	The head of a linked list of the points of the line. It can be NULL
 *				(i.e. draws nothing), can have a single point, or more.
 *				If the last point is the same as the first point, then this pixel is
 *				drawn only once.
 * 
 * @return                      Return the created double circular linked list.
 */
ei_double_linked_point_t *ei_create_double_circular_linked_list(const ei_linked_point_t *first_point);

/**
 * \brief   Free memory space use by table of edge.
 *
 * @param	tc 	The table of edge.
 */
void        free_tc(ei_edge_t **tc, ei_size_t *size);

/**
 * \brief   Free memory space use by a TCA.
 *
 * @param	tc 	The TCA.
 */
void        free_tca(ei_edge_t *tca);

/**
 * \brief   Free memory of linked list of edge.
 *
 * @param	first_egde 	A pointer toward the fisrt edge of the list.
 */
void        free_edge(ei_edge_t *first_egde);

/**
 * \brief   Free memory of double linked list of points.
 *
 * @param	first_point 	A pointer toward the fisrt point of the list.
 */
void        free_double_linked_point(ei_double_linked_point_t *first_point);

/**
 * \brief   Sort (merge sort -> O(nlog(n))) the TCA by ascending abscissa.
 *
 * @param	tca             The TCA to sort.
 */
void        sort_tca(ei_edge_t *tca);




#endif  /* EI_EDGE_H */
