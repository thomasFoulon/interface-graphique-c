#define PI 3.14159265358979323846

#include <stdlib.h>
#include <stdio.h>

#include "ei_tools.h"
#include "ei_types.h"
#include "ei_types.h"
#include "ei_utils.h"
#include "ei_draw.h"
#include "ei_shape.h"
#include "ei_widgetfunctions.h"



void test_print(ei_surface_t surface, ei_rect_t* clipper)
{
    ei_color_t          color       = {255, 0, 0, 255};

    ei_point_t          a           = {2, 1};
    ei_point_t          b           = {5, 1};
    ei_point_t          c           = {4, 2};
    ei_point_t          d           = {1, 3};

    ei_linked_point_t   sd          = {d, NULL};
    ei_linked_point_t   sc          = {c, &sd};
    ei_linked_point_t   sb          = {b, &sc};
    ei_linked_point_t   sa          = {a, &sb};

    ei_draw_polygon(surface, &sa, color, clipper);
}

void test_graphic_square(ei_surface_t surface, ei_rect_t* clipper)
{
    ei_color_t          color       = {255, 0, 70, 100};

    ei_point_t          a           = {200, 200};
    ei_point_t          b           = {700, 200};
    ei_point_t          c           = {700, 600};
    ei_point_t          d           = {200, 600};

    ei_linked_point_t   sd          = {d, NULL};
    ei_linked_point_t   sc          = {c, &sd};
    ei_linked_point_t   sb          = {b, &sc};
    ei_linked_point_t   sa          = {a, &sb};

    ei_draw_polygon(surface, &sa, color, clipper);

}

void test_graphic_crown(ei_surface_t surface, ei_rect_t* clipper)
{
    ei_color_t          color       = {40, 150, 200, 150};

    ei_point_t          a           = {200, 100};
    ei_point_t          b           = {300, 200};
    ei_point_t          c           = {450, 0};
    ei_point_t          d           = {600, 200};
    ei_point_t          e           = {700, 100};
    ei_point_t          f           = {700, 400};
    ei_point_t          g           = {200, 400};

    ei_linked_point_t   sg          = {g, NULL};
    ei_linked_point_t   sf          = {f, &sg};
    ei_linked_point_t   se          = {e, &sf};
    ei_linked_point_t   sd          = {d, &se};
    ei_linked_point_t   sc          = {c, &sd};
    ei_linked_point_t   sb          = {b, &sc};
    ei_linked_point_t   sa          = {a, &sb};

    ei_draw_polygon(surface, &sa, color, clipper);
}

//void test_graphic_button(ei_surface_t surface, ei_rect_t* clipper)
//{
//    ei_point_t          point       = ei_point(350, 400);
//    ei_size_t           size        = ei_size(200, 100);
//    ei_rect_t           rect        = ei_rect(point, size);
//
//    ei_color_t          color_top       = {50, 50, 50, 255};
//    ei_color_t          color_bottom    = {150, 150, 150, 255};
//    ei_color_t          color_center    = {0, 0, 0, 255};
//
//    ei_draw_button(surface, rect, 10, 10, color_top, color_bottom, color_center, clipper);
//}

void test_graphic_disc(ei_surface_t surface, ei_rect_t* clipper)
{
    ei_color_t color = {100, 255, 100, 200};

    ei_point_t center = (ei_point_t){.x = 400, .y = 400};

    ei_bool_t octants[8] = {EI_TRUE, EI_TRUE, EI_TRUE, EI_TRUE, EI_TRUE, EI_TRUE, EI_TRUE, EI_TRUE};
    ei_linked_point_t *cercle = ei_get_arc(center, 70, octants);

    ei_draw_polygon(surface, cercle, color, clipper);
    
    free_linked_point(cercle);
}

int ei_main(int argc, char** argv)
{
    ei_size_t       win_size        = ei_size(1000, 800);
    ei_surface_t    main_window     = NULL;
    ei_color_t      white           = { 0xff, 0xff, 0xff, 0xff };
    ei_rect_t       clipper         = ei_rect(ei_point(0, 0), ei_size(1000, 800));

    hw_init();

    main_window = hw_create_window(&win_size, EI_FALSE);

    // Lock the drawing surface, paint it white
    hw_surface_lock(main_window);
    ei_fill(main_window, &white, &clipper);

    // Draw polygon
    //    test_print(main_window, &clipper);
    test_graphic_square(main_window, &clipper);
    test_graphic_crown(main_window, &clipper);
    //    test_graphic_button(main_window, &clipper);
    test_graphic_disc(main_window, &clipper);


    // Unlock and update the surface
    hw_surface_unlock(main_window);
    hw_surface_update_rects(main_window, NULL);

    // Wait for a character on command line
    getchar();

    hw_quit();
    return (EXIT_SUCCESS);
}
