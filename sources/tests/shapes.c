#include <stdio.h>
#include <stdlib.h>

#include "ei_shape.h"
#include "ei_utils.h"
#include "ei_tools.h"
#include "ei_print.h"

#define PI 3.14159265358979323846


void test_arc()
{
    printf("/ Test arc /\n");
    ei_point_t center = (ei_point_t){.x = 0, .y = 0};
    ei_bool_t octants[8] = {EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_FALSE, EI_TRUE, EI_TRUE};
    ei_linked_point_t *list = ei_get_arc(center, 20, octants);
    print_ei_linked_point(list);
    free_linked_point(list);
}


void test_rounded_frame()
{
    printf("/ Test rounded frame /\n");
    ei_point_t top_left = ei_point(100, 100);
    ei_size_t size = {100, 70};
    ei_rect_t rect = ei_rect(top_left, size);
    ei_linked_point_t *list = ei_get_rounded_frame(rect, 10, BOTTOM);
    print_ei_linked_point(list);
    free_linked_point(list);
}


int main() {
    test_arc();
    test_rounded_frame();
    return (EXIT_SUCCESS);
}
